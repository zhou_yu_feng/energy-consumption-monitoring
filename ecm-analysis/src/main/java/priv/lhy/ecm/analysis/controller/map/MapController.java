package priv.lhy.ecm.analysis.controller.map;

import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import priv.lhy.common.controller.AbsBasicController;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author: lihy
 * date: 2019/7/19 9:40
 * description:
 */
@RestController
public class MapController extends AbsBasicController {

    @GetMapping("/map/getMap/{mapName}")
    public Map<String, Object> getMap(@PathVariable String mapName) {
        Map<String, Object> result = new HashMap<>();
        try {
            File file = ResourceUtils.getFile("classpath:static/map/" + mapName+".json");
            String jsonData = this.jsonRead(file);
            result.put("code", "000000");
            result.put("data", jsonData);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            result.put("code", "000001");
        }
        return result;
    }

    private String jsonRead(File file) {
        Scanner scanner = null;
        StringBuilder buffer = new StringBuilder();
        try {
            scanner = new Scanner(file, "utf-8");
            while (scanner.hasNextLine()) {
                buffer.append(scanner.nextLine());
            }
        } catch (Exception e) {

        } finally {
            if (scanner != null) {
                scanner.close();
            }
        }
        return buffer.toString();
    }
}
