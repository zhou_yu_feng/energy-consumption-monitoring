package priv.lhy.ecm.analysis.utils;

import priv.lhy.common.utils.DateUtil;
import priv.lhy.common.utils.StringUtil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

/**
 * @author: lihy
 * date: 2019/7/18 10:08
 * description:
 */
public class AnalysisUtil {

    public static Object[][] transfer(List list, String cloFields, String cloNames) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        String[] cols = cloFields.split(",");
        String[] colNames = cloNames.split(",");
        Object[][] analyseData = new Object[list.size() + 1][cols.length + 1];

        analyseData[0][0] = "title";

        for (int i = 0; i <= list.size(); i++) {
            if (i != 0) {
                Object bean = list.get(i - 1);
                Method method = bean.getClass().getMethod("getCollecttime");
                Object val = method.invoke(bean);
                analyseData[i][0] = DateUtil.dateCustomStr((Date) val, "yyyy-MM-dd HH:mm:ss");
            }
            for (int j = 1; j <= cols.length; j++) {
                if (i == 0) {
                    analyseData[i][j] = colNames[j - 1];
                }
                if (i != 0) {
                    Object bean = list.get(i - 1);
                    String col = cols[j - 1];

                    Method method = bean.getClass().getMethod("get" + StringUtil
                            .firstCharUpperCase(col));
                    Object val = method.invoke(bean);
                    analyseData[i][j] = val;

                }
            }
        }
        return analyseData;
    }
}
