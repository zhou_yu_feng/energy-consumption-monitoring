package priv.lhy.ecm.analysis.controller.collector;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import priv.lhy.common.controller.AbsBasicController;
import priv.lhy.ecm.analysis.dto.elec.ElecQueryRequest;
import priv.lhy.ecm.analysis.dto.elec.ElecQueryResponse;
import priv.lhy.ecm.analysis.entity.elec.Electricity;
import priv.lhy.ecm.analysis.service.elec.IElecService;
import priv.lhy.ecm.analysis.utils.AnalysisUtil;
import priv.lhy.ecm.basic.dto.companyShow.CompanyShowQueryRequest;
import priv.lhy.ecm.basic.dto.companyShow.CompanyShowQueryResponse;
import priv.lhy.ecm.basic.entity.company.CompanyShow;
import priv.lhy.ecm.basic.service.company.ICompanyShowService;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

/**
 * @author: lihy
 * date: 2019/7/8 15:25
 * description:
 */
@RestController
public class ElecController extends AbsBasicController {

    @Reference
    private IElecService elecService;

    @Reference
    private ICompanyShowService showService;

    @GetMapping("/getElecs")
    public Map<String, Object> getElecs(HttpServletRequest servletRequest) {
        CompanyShowQueryRequest showRequest = new CompanyShowQueryRequest();
        showRequest.setCdid(Long.parseLong(servletRequest.getParameter("cdid")));
        CompanyShowQueryResponse showResponse = new CompanyShowQueryResponse();
        showResponse = (CompanyShowQueryResponse) showService.selectById(showResponse, showRequest, showRequest.getCdid());

        CompanyShow cs = (CompanyShow) showResponse.getData();

        ElecQueryRequest request = new ElecQueryRequest();
        request.setCompanyCode(Integer.parseInt(servletRequest.getParameter("companyCode")));
        request.setShowColumns(cs.getColsfield());
        request.setBegin(servletRequest.getParameter("begin"));
        request.setEnd(servletRequest.getParameter("end"));

        ElecQueryResponse response = (ElecQueryResponse) elecService.selectByTime(request);
        List<Electricity> electricities = (List<Electricity>) response.getData();

        Object[][] analyseData = null;
        try {
            analyseData = AnalysisUtil.transfer(electricities, cs.getColsfield(), cs
                    .getColsname());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        /*String[] cols = cs.getColsfield().split(",");
        String[] colNames = cs.getColsname().split(",");
        Object[][] analyseData = new Object[electricities.size() + 1][cols.length + 1];

        analyseData[0][0] = "title";

        for (int i = 0; i <= electricities.size(); i++) {
            if (i != 0) {
                analyseData[i][0] = DateUtil.dateCustomStr(electricities.get(i - 1)
                        .getCollecttime(), "yyyy-MM-dd HH:mm:ss");
            }
            for (int j = 1; j <= cols.length; j++) {
                if (i == 0) {
                    analyseData[i][j] = colNames[j-1];
                }
                if (i != 0) {
                    Electricity collector = electricities.get(i - 1);
                    String col = cols[j-1];
                    try {
                        Method method = collector.getClass().getMethod("get" + StringUtil
                                .firstCharUpperCase(col));
                        Object val = method.invoke(collector);
                        analyseData[i][j] = val;
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }

        }*/


        Map<String, Object> result = super.assemblyResult(response);
        result.put("data", analyseData);
        return result;


    }
}
