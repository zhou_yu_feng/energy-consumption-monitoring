package priv.lhy.ecm.analysis.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: lihy
 * date: 2019/7/8 14:15
 * description:
 */
@RestController
public class AnalysisExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public static Object handleException(Exception e){
        Map<String, Object> map = new HashMap<>();
        map.put("code", "999999");
        map.put("msg", e.getMessage());
        return map;
    }

    @ExceptionHandler(value = AnalysisException.class)
    public static Object handleAnalysisException(AnalysisException e){
        Map<String, Object> map = new HashMap<>();
        map.put("code", e.getCode());
        map.put("msg", e.getMsg());
        map.put("url", e.getRequest().getUrl());
        map.put("requestTime", e.getRequest().getRequestTime().toString());
        return map;
    }

}
