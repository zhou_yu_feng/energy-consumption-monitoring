package priv.lhy.ecm.analysis;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableDubboConfiguration
public class EcmAnalysisApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcmAnalysisApplication.class, args);
	}

}
