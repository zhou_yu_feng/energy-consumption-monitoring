package priv.lhy.ecm.analysis.controller.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author: lihy
 * date: 2019/7/9 10:27
 * description:
 */
@Controller
public class HtmlController {

    @GetMapping("/index")
    public String index(HttpServletRequest request){
        System.out.println(request.getSession().getAttribute("uid") +","
                + request.getSession().getAttribute("username"));
        return "index";
    }

    @GetMapping("/main")
    public String main(){
        return "main";
    }

    @GetMapping("/mainPage")
    public String mainPage(){
        return "include/map";
    }

    @GetMapping("/test")
    public String test(){
        return "test";
    }
}
