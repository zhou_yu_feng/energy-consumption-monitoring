drop table if exists em_userinfo;

/*==============================================================*/
/* Table: em_userinfo                                           */
/*==============================================================*/
create table em_userinfo
(
   uid                  bigint not null auto_increment,
   userid               varchar(20),
   pwd                  char(32),
   realname             varchar(10),
   companyid            bigint,
   accessid             varchar(36),
   status               tinyint,
   primary key (uid)
);

alter table em_userinfo comment '用户信息';


drop table if exists em_companyinfo;

/*==============================================================*/
/* Table: em_companyinfo                                        */
/*==============================================================*/
create table em_companyinfo
(
   cid                  bigint not null auto_increment,
   cname                varchar(100),
   ctype                bigint,
   area                 decimal(10,2),
   airarea              decimal(10,2),
   address              varchar(100),
   longitude            decimal(12,6),
   latitude             decimal(12,6),
   zipcode              char(6),
   aid                  char(6),
   street               varchar(200),
   timezone             bigint,
   detailed             longtext,
   pic                  varchar(200),
   status               tinyint,
   svg                  varchar(200),
   usernum              int,
   topological          varchar(200),
   etype                varchar(20),
   primary key (cid)
);

alter table em_companyinfo comment '公司信息';

drop table if exists em_menuinfo;

/*==============================================================*/
/* Table: em_menuinfo                                           */
/*==============================================================*/
create table em_menuinfo
(
   mid                  char(6) not null,
   mname                varchar(20),
   nurl                 varchar(200),
   parent               char(6),
   sort                 int,
   status               tinyint,
   mtype                bigint,
   ioc                  varchar(50),
   primary key (mid)
);

alter table em_menuinfo comment '菜单信息';


drop table if exists em_access_menu;

/*==============================================================*/
/* Table: em_access_menu                                        */
/*==============================================================*/
create table em_access_menu
(
   amid                 bigint not null auto_increment,
   aid                  bigint,
   mid                  char(6),
   primary key (amid)
);

alter table em_access_menu comment '权限菜单';


drop table if exists em_accessinfo;

/*==============================================================*/
/* Table: em_accessinfo                                         */
/*==============================================================*/
create table em_accessinfo
(
   aid                  bigint not null,
   aname                varchar(30),
   status               tinyint,
   indexpage            varchar(100),
   primary key (aid)
);

alter table em_accessinfo comment '权限信息';


drop table if exists em_company_prt;

/*==============================================================*/
/* Table: em_company_prt                                        */
/*==============================================================*/
create table em_company_prt
(
   cpid                 bigint not null auto_increment,
   cid                  bigint,
   ctype                bigint,
   cvalue               varchar(20),
   status               tinyint,
   primary key (cpid)
);

alter table em_company_prt comment '企业参数';


drop table if exists em_dictionarytype;

/*==============================================================*/
/* Table: em_dictionarytype                                     */
/*==============================================================*/
create table em_dictionarytype
(
   dtid                 bigint not null,
   dtname               varchar(100),
   status               tinyint,
   primary key (dtid)
);

alter table em_dictionarytype comment '字典类型';


drop table if exists em_dictionaryinfo;

/*==============================================================*/
/* Table: em_dictionaryinfo                                     */
/*==============================================================*/
create table em_dictionaryinfo
(
   did                  bigint not null,
   dtid                 smallint,
   dname                varchar(100),
   status               tinyint,
   sort                 tinyint,
   primary key (did)
);

alter table em_dictionaryinfo comment '字典信息';


drop table if exists em_company_show;

/*==============================================================*/
/* Table: em_company_show                                       */
/*==============================================================*/
create table em_company_show
(
   cdid                 bigint not null auto_increment,
   cid                  bigint,
   dname                varchar(30),
   ttable               varchar(50),
   tname                varchar(100),
   tfield               varchar(100),
   sort                 tinyint,
   status               tinyint,
   unit                 varchar(20),
   primary key (cdid)
);

alter table em_company_show comment '企业显示数据';



drop table if exists em_areainfo;

/*==============================================================*/
/* Table: em_areainfo                                           */
/*==============================================================*/
create table em_areainfo
(
   aid                  char(6) not null,
   anme                 varchar(50),
   status               tinyint,
   primary key (aid)
);

alter table em_areainfo comment '行政区划';


drop table if exists em_equipment;

/*==============================================================*/
/* Table: em_equipment                                          */
/*==============================================================*/
create table em_equipment
(
   eid                  bigint not null auto_increment,
   ename                varchar(100),
   eaddress             varchar(30),
   model                varchar(100),
   etype                bigint,
   edesc                longtext,
   cid                  bigint,
   status               tinyint,
   grade                bigint,
   xcoord               int,
   ycoord               int,
   position             varchar(50),
   nodenum              int,
   usernum              int,
   primary key (eid)
);

alter table em_equipment comment '设备信息';


drop table if exists em_equipment_str;

/*==============================================================*/
/* Table: em_equipment_str                                      */
/*==============================================================*/
create table em_equipment_str
(
   esid                 bigint not null auto_increment,
   eid                  bigint,
   ename                varchar(200),
   parent               bigint,
   cid                  bigint,
   etype                bigint,
   primary key (esid)
);

alter table em_equipment_str comment '设备结构';

