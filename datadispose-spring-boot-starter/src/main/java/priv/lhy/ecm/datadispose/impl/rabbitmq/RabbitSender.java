package priv.lhy.ecm.datadispose.impl.rabbitmq;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author: lihy
 * date: 2019/7/31 10:54
 * description:
 */

@ConditionalOnClass(name="org.springframework.amqp.rabbit.AsyncRabbitTemplate")
public class RabbitSender {

    @Value("${data.dispose.rabbit.directexchange}")
    private String exchange;

    @Value("${data.dispose.rabbit.directroutingkey}")
    private String routingKey;

    @Resource
    private AmqpTemplate rabbitTemplate;

    public void sned(String message){
        rabbitTemplate.convertAndSend(exchange, routingKey, message);
    }

}
