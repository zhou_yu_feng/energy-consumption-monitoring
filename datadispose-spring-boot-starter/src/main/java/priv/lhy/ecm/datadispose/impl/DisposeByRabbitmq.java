package priv.lhy.ecm.datadispose.impl;

import org.springframework.beans.factory.annotation.Autowired;
import priv.lhy.ecm.datadispose.AbstractDispose;
import priv.lhy.ecm.datadispose.impl.rabbitmq.RabbitSender;

/**
 * @author: lihy
 * date: 2019/7/30 11:37
 * description:
 */
public class DisposeByRabbitmq extends AbstractDispose {

    @Autowired
    private RabbitSender sender;

    @Override
    public String disposeLegalData(String[] message) {
        sender.sned(message[1] + "|" + message[2] + "|" + message[3] + "|" + message[4]);
        return "ok";
    }
}
