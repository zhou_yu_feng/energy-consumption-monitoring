package priv.lhy.ecm.datadispose.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import pirv.lhy.ecm.collectorAPI.entity.TransferData;
import priv.lhy.common.utils.JsonUtil;
import priv.lhy.common.utils.StringUtil;
import priv.lhy.ecm.datadispose.AbstractDispose;
import priv.lhy.ecm.service.IService;

import java.lang.reflect.Method;
import java.util.List;

/**
 * @author: lihy
 * date: 2019/7/29 16:03
 * description:
 */
public class DisposeByDatabase extends AbstractDispose {

    @Autowired
    private ApplicationContext applicationContext;

    private final String transferPackage = "pirv.lhy.ecm.collectorAPI.entity.transfer";

    private final String entityPackage = "pirv.lhy.ecm.collectorAPI.entity.data";

    @Override
    public String disposeLegalData(String[] message) {
        /**
         * 上传数据格式：
         * token|userCode|uploadType|tableSuffix|jsonData|delimiter
         */
        try {
            //上传数据类型
            String uploadType = message[2].toLowerCase();
            //上传数据传输类
            Class transferClass = Class.forName(transferPackage + ".Transfer" + StringUtil
                    .firstCharUpperCase(uploadType));
            //上传数据实体类
            Class entityClass = Class.forName(entityPackage + "." + StringUtil.firstCharUpperCase
                    (uploadType));
            //将字符串转换为上传数据实例
            TransferData transferData = JsonUtil.json2Object(message[4], transferClass);
            //获取上传数据列表
            List list = transferData.getList();
            //获取对应数据处理service
            IService service = (IService) applicationContext.getBean(uploadType + "Service");
            //service父类注入companyCode
            Method setCompanyCodeMethod = service.getClass().getMethod("setCompanyCode", new Class<?>[]{String.class});
            setCompanyCodeMethod.invoke(service, new Object[]{message[1]});
            //批量写入数据库
            service.batchAdd(message[3], list);

        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
        return "ok";
    }
}
