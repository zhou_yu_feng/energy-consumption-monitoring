package priv.lhy.ecm.datadispose.impl.rabbitmq;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: lihy
 * date: 2019/7/31 9:50
 * description: 配置rabbitMQ template 使用json消息格式
 */
@Configuration
@ConditionalOnClass(name="org.springframework.amqp.rabbit.AsyncRabbitTemplate")
public class RabbitConfig {

   /* @Bean
    public String getDirectExchange( @Value("${data.dispose.rabbit.directexchange}") String exchange){
        return exchange;
    }

    @Bean
    public String getDirectRoutingKey( @Value("${data.dispose.rabbit.directroutingkey}") String routingKey){
        return routingKey;
    }*/

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory factory){
        final RabbitTemplate template = new RabbitTemplate(factory);
        template.setMessageConverter(new SimpleMessageConverter());
        return template;
    }



}
