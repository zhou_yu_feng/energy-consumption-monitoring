package priv.lhy.ecm.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author: lihy
 * date: 2019/6/26 14:02
 * description:
 */
public interface AbsMapper<T> {

    /**
     *  检查表是否存在
     * @param tableName
     * @return
     */
    int existTable(@Param("tableName") String tableName);

    /**
     * 创建表
     * @param tableName
     * @return
     */
    int createTable(@Param("tableName") String tableName);

    /**
     * 创建索引
     * @param tableName
     * @param indexName
     * @return
     */
    int createIndex(@Param("tableName") String tableName, @Param("indexName") String indexName);

    /**
     * 增加单条数据
     * @param t
     * @return
     */
    int insert(@Param("tableName") String tableName, @Param("t") T t);

    /**
     * 批量增加数据
     * @param tableName
     * @param list
     * @return
     */
    int batchInsert(@Param("tableName") String tableName, @Param("list") List<T> list);
}
