package priv.lhy.ecm.mapper;

import org.apache.ibatis.annotations.Mapper;
import pirv.lhy.ecm.collectorAPI.entity.data.Watermeter;

@Mapper
public interface WatermeterMapper extends AbsMapper<Watermeter>{
}