package priv.lhy.ecm.config.DatabaseConfig;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import priv.lhy.ecm.config.DisposeProperties;

import java.io.IOException;
import java.io.StringReader;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author: lihy
 * date: 2019/9/16 11:41
 * description:
 */
@Configuration
@ConditionalOnClass(name = "com.mysql.jdbc.Driver")
public class DataSourceConfiguration {

    private DisposeProperties properties;

    public DataSourceConfiguration(DisposeProperties properties) {
        this.properties = properties;
    }

    @Bean("dataSource")
    public DruidDataSource dataSource() {
        Map<String, Object> prop = properties.getSetting();
        Map<String, Object> sqlPropMap = (Map<String, Object>) prop.get("jdbc");
        //可以在此处调用相关接口获取数据库的配置信息进行 DataSource 的配置
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(sqlPropMap.get("url").toString());
        dataSource.setUsername(sqlPropMap.get("username").toString());
        dataSource.setPassword(sqlPropMap.get("password").toString());
        dataSource.setDriverClassName(sqlPropMap.get("driver").toString());

       /* //初始化时建立物理连接的个数
        dataSource.setInitialSize(Integer.parseInt(sqlPropMap.get("initialSize").toString()));
        //最大连接池数量
        dataSource.setMaxActive(Integer.parseInt(sqlPropMap.get("maxActive").toString()));
        //最小连接池数量
        dataSource.setMinIdle(Integer.parseInt(sqlPropMap.get("minIdle").toString()));
        //获取连接时最大等待时间，单位毫秒。
        dataSource.setMaxWait(Integer.parseInt(sqlPropMap.get("maxWait").toString()));
        //间隔多久进行一次检测，检测需要关闭的空闲连接
        dataSource.setTimeBetweenEvictionRunsMillis(Integer.parseInt(sqlPropMap.get("timeBetweenEvictionRunsMillis").toString()));
        //一个连接在池中最小生存的时间
        dataSource.setMinEvictableIdleTimeMillis(Integer.parseInt(sqlPropMap.get("minEvictableIdleTimeMillis").toString()));
        //用来检测连接是否有效的sql
        dataSource.setValidationQuery(sqlPropMap.get("validationQuery").toString());
        //建议配置为true，不影响性能，并且保证安全性。
        dataSource.setTestWhileIdle(Boolean.parseBoolean(sqlPropMap.get("testWhileIdle").toString()));
        //申请连接时执行validationQuery检测连接是否有效
        dataSource.setTestOnBorrow(Boolean.parseBoolean(sqlPropMap.get("testOnBorrow").toString()));
        dataSource.setTestOnReturn(Boolean.parseBoolean(sqlPropMap.get("testOnReturn").toString()));
        //是否缓存preparedStatement，也就是PSCache，oracle设为true，mysql设为false。分库分表较多推荐设置为false
        dataSource.setPoolPreparedStatements(Boolean.parseBoolean(sqlPropMap.get("poolPreparedStatements").toString()));
        // 打开PSCache时，指定每个连接上PSCache的大小
        dataSource.setMaxPoolPreparedStatementPerConnectionSize(Integer.parseInt(sqlPropMap.get("maxPoolPreparedStatementPerConnectionSize").toString()));*/

        return dataSource;
    }




    @Deprecated
    private Map<String, String> setSqlProperties(String propStr) {
        Map<String, String> map = new HashMap<>();
        Properties proper = new Properties();
        try {
            proper.load(new StringReader(propStr));
            Enumeration enum1 = proper.propertyNames();
            while (enum1.hasMoreElements()) {
                String strKey = (String) enum1.nextElement();
                String strValue = proper.getProperty(strKey);
                map.put(strKey, strValue);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }
}
