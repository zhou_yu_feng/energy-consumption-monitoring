package priv.lhy.ecm.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * @author: lihy
 * date: 2019/8/4 17:08
 * description:
 */
@ConfigurationProperties(prefix = DisposeProperties.DISPOSE_PREFIX)
public class DisposeProperties {

    public final static String DISPOSE_PREFIX = "data.dispose";

    private Map<String, Object> setting;

    public static String getDisposePrefix() {
        return DISPOSE_PREFIX;
    }

    public Map<String, Object> getSetting() {
        return setting;
    }

    public void setSetting(Map<String, Object> setting) {
        this.setting = setting;
    }
}
