package priv.lhy.ecm.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import priv.lhy.ecm.datadispose.IDispose;
import priv.lhy.ecm.datadispose.impl.DisposeByDatabase;
import priv.lhy.ecm.datadispose.impl.DisposeByKafka;
import priv.lhy.ecm.datadispose.impl.DisposeByRabbitmq;
import priv.lhy.ecm.datadispose.impl.kafka.KafkaSender;
import priv.lhy.ecm.datadispose.impl.rabbitmq.RabbitSender;

/**
 * @author: lihy
 * date: 2019/8/4 16:41
 * description:
 */
@Configuration
public class DisposeAutoConfiguration {

    @Bean
    @Primary
    @ConditionalOnClass(name="com.mysql.jdbc.Driver")
    public IDispose databaseDispose(){
        return new DisposeByDatabase();
    }

    @Bean
    @ConditionalOnClass(name="org.springframework.amqp.rabbit.AsyncRabbitTemplate")
    public IDispose rabbitmqDispose(){
        return new DisposeByRabbitmq();
    }

    @Bean
    @ConditionalOnClass(name = "org.springframework.kafka.KafkaException")
    public IDispose kafkaDispose(){
        return new DisposeByKafka();
    }

    @Bean
    @ConditionalOnClass(name="org.springframework.amqp.rabbit.AsyncRabbitTemplate")
    public RabbitSender rabbitSender(){
        return new RabbitSender();
    }

    @Bean
    @ConditionalOnClass(name = "org.springframework.kafka.KafkaException")
    public KafkaSender kafkaSender(){
        return new KafkaSender();
    }
}
