package priv.lhy.ecm.config.DatabaseConfig;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.*;

/**
 * @author: lihy
 * date: 2019/8/4 17:50
 * description:
 */
@Configuration
@ConditionalOnClass(name="com.mysql.jdbc.Driver")
@ComponentScan({"priv.lhy.ecm.service"})
public class DatabaseServiceAutoConfiguration {


    /*@Bean("electricityService")
    @Lazy(value = false)
    @Primary
    @ConditionalOnClass(name="com.mysql.jdbc.Driver")
    public IService elecService(){
        return new ElectricityServiceImpl();
    }

    @Bean("watermeterService")
    @Lazy(value = false)
    @ConditionalOnClass(name="com.mysql.jdbc.Driver")
    public IService watermeterService(){
        return new WatermeterServiceImpl();
    }*/

}
