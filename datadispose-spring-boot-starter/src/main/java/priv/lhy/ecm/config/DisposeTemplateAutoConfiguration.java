package priv.lhy.ecm.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import priv.lhy.ecm.DisposeTemplte;
import priv.lhy.ecm.config.DatabaseConfig.DatabaseServiceAutoConfiguration;
import priv.lhy.ecm.datadispose.IDispose;

/**
 * @author: lihy
 * date: 2019/8/4 17:05
 * description:
 */
@Configuration
@Import({DisposeAutoConfiguration.class, DatabaseServiceAutoConfiguration.class})
public class DisposeTemplateAutoConfiguration {

    @Bean
    public DisposeTemplte getDisposeTemplate(IDispose dispose, DisposeProperties properties){
        return new DisposeTemplte(dispose, properties);
    }

}
