package priv.lhy.ecm;

import priv.lhy.ecm.config.DisposeProperties;
import priv.lhy.ecm.datadispose.IDispose;

/**
 * @author: lihy
 * date: 2019/8/4 17:00
 * description:
 */

public class DisposeTemplte {

    private IDispose dispose;

    private DisposeProperties properties;

    public DisposeTemplte(IDispose dispose, DisposeProperties properties) {
        this.dispose = dispose;
        this.properties = properties;
    }

    public String disposeLegalData(String[] message){
        return dispose.disposeLegalData(message);
    }

    public String disposeIllegalData(String ip) {
        return dispose.disposeIllegalData(ip);
    }

}
