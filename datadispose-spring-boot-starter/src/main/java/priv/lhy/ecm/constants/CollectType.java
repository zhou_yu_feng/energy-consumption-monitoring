package priv.lhy.ecm.constants;

import pirv.lhy.ecm.collectorAPI.entity.data.Electricity;

/**
 * author: lihy
 * date: 2019/6/26 14:50
 * description:
 */
public enum CollectType {
    ELECTRICITY(Electricity.class, "em_electricity_");

    private final Class type;
    private final String tableName;

    CollectType(Class type, String tableName) {
        this.type = type;
        this.tableName = tableName;
    }

    public Class getType() {
        return type;
    }

    public String getTableName() {
        return tableName;
    }
}
