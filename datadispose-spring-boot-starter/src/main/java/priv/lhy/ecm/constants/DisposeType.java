package priv.lhy.ecm.constants;

/**
 * @author: lihy
 * date: 2019/7/29 17:14
 * description:
 */
public enum DisposeType {
    DATABASE, RABBITMQ, KAFKA;
}
