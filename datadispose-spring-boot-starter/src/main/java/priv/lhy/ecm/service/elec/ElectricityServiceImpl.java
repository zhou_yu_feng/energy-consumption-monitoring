package priv.lhy.ecm.service.elec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.stereotype.Service;
import pirv.lhy.ecm.collectorAPI.entity.data.Electricity;
import priv.lhy.ecm.mapper.ElectricityMapper;
import priv.lhy.ecm.service.AbsService;
import priv.lhy.ecm.service.IService;

import java.util.List;

/**
 * author: lihy
 * date: 2019/6/26 15:01
 * description:
 */
@Service("electricityService")
//@ConditionalOnClass(name="com.mysql.jdbc.Driver")
public class ElectricityServiceImpl extends AbsService<Electricity> implements IService<Electricity> {

    @Autowired
    private ElectricityMapper elecMapper;

    @Override
    public int add(Electricity electricity) {
        super.createTable(elecMapper, "");
        return elecMapper.insert(super.getTableName(), electricity);
    }

    @Override
    public int batchAdd(List<Electricity> list) {
        super.createTable(elecMapper, "");
        return elecMapper.batchInsert(super.getTableName(), list);
    }

    @Override
    public int batchAdd(String tableSuffix, List<Electricity> list) {
        super.createTable(elecMapper, tableSuffix);
        return elecMapper.batchInsert(super.getTableName(), list);
    }
}
