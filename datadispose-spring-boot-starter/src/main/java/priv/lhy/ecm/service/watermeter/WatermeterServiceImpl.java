package priv.lhy.ecm.service.watermeter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.stereotype.Service;
import pirv.lhy.ecm.collectorAPI.entity.data.Watermeter;
import priv.lhy.ecm.mapper.WatermeterMapper;
import priv.lhy.ecm.service.AbsService;
import priv.lhy.ecm.service.IService;

import java.util.List;

/**
 * @author: lihy
 * date: 2019/7/1 12:09
 * description:
 */
@Service("watermeterService")
//@ConditionalOnClass(name="com.mysql.jdbc.Driver")
public class WatermeterServiceImpl extends AbsService<Watermeter> implements IService<Watermeter> {

    @Autowired
    private WatermeterMapper watermeterMapper;

    @Override
    public int add(Watermeter watermeter) {
        super.createTable(watermeterMapper, "");
        return watermeterMapper.insert(super.getTableName(), watermeter);
    }

    @Override
    public int batchAdd(List<Watermeter> list) {
        super.createTable(watermeterMapper, "");
        return watermeterMapper.batchInsert(super.getTableName(), list);
    }

    @Override
    public int batchAdd(String tableSuffix, List<Watermeter> list) {
        super.createTable(watermeterMapper, "");
        return watermeterMapper.batchInsert(super.getTableName(), list);
    }
}
