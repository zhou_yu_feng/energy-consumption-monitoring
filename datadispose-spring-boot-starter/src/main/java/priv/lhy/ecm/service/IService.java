package priv.lhy.ecm.service;

import java.util.List;

/**
 * author: lihy
 * date: 2019/6/26 14:42
 * description:
 */
public interface IService<T> {

    /**
     * 增加单条数据
     * @param t
     * @return
     */
    int add(T t);

    /**
     * 批量增加数据
     * @param list
     * @return
     */
    int batchAdd(List<T> list);

    /**
     * 指定表后缀批量增加数据
     * @param tableSuffix
     * @param list
     * @return
     */
    int batchAdd(String tableSuffix, List<T> list);
}
