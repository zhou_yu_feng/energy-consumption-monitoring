package priv.lhy.ecm.collectorConsumer;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

//@SpringBootApplication//(scanBasePackages = {"priv.lhy.ecm"})
//@MapperScan({"priv.lhy.ecm"})
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class CollectorConsumerApplication {



	public static void main(String[] args) {
		SpringApplication.run(CollectorConsumerApplication.class, args);
	}

}
