package priv.lhy.user.mapper;

import org.apache.ibatis.annotations.*;
import priv.lhy.user.dal.Userinfo;

import java.util.List;

/**
 * author: lihy
 * date: 2019/4/3 13:53
 * description:
 */
@Mapper
public interface UserMapper {

    @Select("SELECT * FROM em_userinfo WHERE 1=1 ${condition}")
    List<Userinfo> getUsers(@Param("condition") String condition);

    @Insert("INSERT INTO em_userinfo(loginid, pwd, realname, companyid, accessid, status) VALUES" +
            "(#{loginid}, #{pwd}, #{realname}, #{companyid}, #{accessid}, #{status})")
    @Options(useGeneratedKeys = true)
    long addUser(Userinfo userinfo);
}
