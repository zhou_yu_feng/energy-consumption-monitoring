package priv.lhy.user.service;

import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;
import priv.lhy.common.utils.MD5Util;
import priv.lhy.user.constants.UserResponseCode;
import priv.lhy.user.dal.Userinfo;
import priv.lhy.user.dto.UserLoginRequest;
import priv.lhy.user.dto.UserLoginResponse;
import priv.lhy.user.exception.UserException;
import priv.lhy.user.mapper.UserMapper;

import java.util.List;

/**
 * author: lihy
 * date: 2019/4/3 13:56
 * description:
 */
@Service(interfaceClass = IUserCoreService.class)
@Component
public class UserCoreServiceImpl implements IUserCoreService {

    @Autowired
    UserMapper userMapper;


    @Override
    public UserLoginResponse login(UserLoginRequest request) {
        UserLoginResponse response = new UserLoginResponse();
        try {
            String condition = "";
            /*condition += " and loginid = '" + request.getUserName() + "' and pwd='" + MD5Util
                    .getMD5Str(request.getPassword(), request.getUserName()) + "'";*/
            condition += " and loginid = '" + request.getUserName() + "' and pwd='" + request.getPassword() + "'";

            List<Userinfo> users = userMapper.getUsers(condition);

            if(null!=users && users.size()==1){
                response.setUid((int)users.get(0).getUid());
                response.setUsername(users.get(0).getRealname());
                response.setCode(UserResponseCode.SUCCESS.getCode());
                response.setMsg(UserResponseCode.SUCCESS.getMsg());
            }else{
                response.setCode(UserResponseCode.USERORPASSWORD_ERRROR.getCode());
                response.setMsg(UserResponseCode.USERORPASSWORD_ERRROR.getMsg());
            }
        } catch (Exception e) {
            throw new UserException(UserResponseCode.SYSTEM_BUSY.getCode(), UserResponseCode
                    .SYSTEM_BUSY.getMsg(), request);
        }

        return response;
    }
}
