package priv.lhy.user.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import priv.lhy.common.abs.AbstractRequest;
import priv.lhy.user.dto.UserLoginRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * author: lihy
 * date: 2019/4/4 9:40
 * description:
 */
@ControllerAdvice
public class UserExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(UserExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    public Object handleException(Exception e, AbstractRequest req) {
        LOG.error("url: {}, msg: {}", req.getUrl(), e.getMessage());
        Map<String, Object> map = new HashMap<>();
        map.put("code", "999999");
        map.put("msg", e.getMessage());
        return map;
    }

    /*@ExceptionHandler(value = Exception.class)
    public Object handleException(Exception e) {
        LOG.error("msg: {}", e.getMessage());
        Map<String, Object> map = new HashMap<>();
        map.put("code", "999999");
        map.put("msg", e.getMessage());
        return map;
    }*/

    @ExceptionHandler(value = UserException.class)
    public Object handleUserException(UserException e, AbstractRequest req) {
        LOG.error("url: {}, msg: {}", req.getUrl(), e.getMessage());
        Map<String, Object> map = new HashMap<>();
        map.put("code", e.getCode());
        map.put("msg", e.getMessage());
        return map;
    }
}
