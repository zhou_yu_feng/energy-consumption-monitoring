package priv.lhy.user.dto;

import priv.lhy.common.abs.AbstractResponse;

/**
 * author: lihy
 * date: 2019/4/2 14:30
 * description: 用户登录响应
 */
public class UserLoginResponse extends AbstractResponse {

    private String username;
    private Long accessId;
    private Object data;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Long getAccessId() {
        return accessId;
    }

    public void setAccessId(Long accessId) {
        this.accessId = accessId;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
