package priv.lhy.user.dto;

import priv.lhy.common.abs.AbstractRequest;

/**
 * author: lihy
 * date: 2019/4/2 14:10
 * description: 登录请求
 */
public class UserLoginRequest extends AbstractRequest {

    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
