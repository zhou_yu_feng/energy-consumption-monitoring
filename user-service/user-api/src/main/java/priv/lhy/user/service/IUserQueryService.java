package priv.lhy.user.service;

import org.springframework.transaction.annotation.Transactional;
import priv.lhy.user.dto.UserQueryRequest;
import priv.lhy.user.dto.UserQueryResponse;
import priv.lhy.user.exception.UserException;

/**
 * author: lihy
 * date: 2019/4/2 14:50
 * description:
 */
@Transactional
public interface IUserQueryService {



    /**
     * 根据用户id来查询用户信息
     * @param request
     * @return
     */
    UserQueryResponse getUserById(UserQueryRequest request) throws Exception;

    /**
     * 根据用户id来查询用户信息
     * @param request
     * @return
     */
    UserQueryResponse getUserByIdWithLimiter(UserQueryRequest request) throws Exception;
}
