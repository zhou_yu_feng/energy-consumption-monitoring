package priv.lhy.user.exception;

import priv.lhy.common.abs.AbstractRequest;
import priv.lhy.common.exception.AbstractException;

/**
 * author: lihy
 * date: 2019/4/4 9:37
 * description:
 */
public class UserException extends AbstractException {

    public UserException(String code, String msg, AbstractRequest request) {
        super(code, msg, request);
    }
}
