package priv.lhy.user.service;

import org.springframework.transaction.annotation.Transactional;
import priv.lhy.user.dto.UserLoginResponse;
import priv.lhy.user.dto.UserLoginRequest;

/**
 * author: lihy
 * date: 2019/4/2 14:50
 * description:
 */
@Transactional
public interface IUserCoreService {
    /**
     * 用户登录
     * @param request
     * @return
     */
    UserLoginResponse login(UserLoginRequest request);
}
