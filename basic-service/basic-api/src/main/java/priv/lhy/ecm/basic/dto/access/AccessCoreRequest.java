package priv.lhy.ecm.basic.dto.access;

import priv.lhy.common.abs.AbstractRequest;
import priv.lhy.ecm.basic.entity.acess.AccessMenu;
import priv.lhy.ecm.basic.entity.acess.Accessinfo;

/**
 * author: lihy
 * date: 2019/5/6 13:05
 * description:
 */
public class AccessCoreRequest extends AbstractRequest {

    private Accessinfo access;
    private AccessMenu menu;

    public Accessinfo getAccess() {
        return access;
    }

    public void setAccess(Accessinfo access) {
        this.access = access;
    }

    public AccessMenu getMenu() {
        return menu;
    }

    public void setMenu(AccessMenu menu) {
        this.menu = menu;
    }
}
