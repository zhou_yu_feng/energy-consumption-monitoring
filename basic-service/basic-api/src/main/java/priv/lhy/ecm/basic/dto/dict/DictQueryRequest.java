package priv.lhy.ecm.basic.dto.dict;

import priv.lhy.common.abs.AbstractRequest;

/**
 * author: lihy
 * date: 2019/4/10 15:15
 * description:
 */
public class DictQueryRequest extends AbstractRequest {

    private long did;
    private long dtid;
    private String sort;

    public long getDid() {
        return did;
    }

    public void setDid(long did) {
        this.did = did;
    }

    public long getDtid() {
        return dtid;
    }

    public void setDtid(long dtid) {
        this.dtid = dtid;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
