package priv.lhy.ecm.basic.service.company;

import org.springframework.transaction.annotation.Transactional;
import priv.lhy.ecm.basic.dto.company.CompanyCoreRequest;
import priv.lhy.ecm.basic.dto.company.CompanyCoreResponse;
import priv.lhy.ecm.basic.dto.company.CompanyQueryRequest;
import priv.lhy.ecm.basic.dto.company.CompanyQueryResponse;
import priv.lhy.ecm.basic.entity.company.Companyinfo;
import priv.lhy.ecm.basic.service.IService;

/**
 * author: lihy
 * date: 2019/5/10 14:01
 * description:
 */
@Transactional
public interface ICompanyService extends IService<Companyinfo> {

   // CompanyCoreResponse addCompany(CompanyCoreRequest request);

   //CompanyCoreResponse updateCompany(CompanyCoreRequest request);

    //CompanyQueryResponse getCompanyById(CompanyQueryRequest request);

    CompanyQueryResponse getCompanyList(CompanyQueryRequest request);
}
