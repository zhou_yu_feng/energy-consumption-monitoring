package priv.lhy.ecm.basic.service.access;

import org.springframework.transaction.annotation.Transactional;
import priv.lhy.ecm.basic.dto.access.*;
import priv.lhy.ecm.basic.entity.acess.Accessinfo;
import priv.lhy.ecm.basic.service.IService;

/**
 * author: lihy
 * date: 2019/5/6 12:53
 * description:
 */
@Transactional
public interface IAccessService extends IService<Accessinfo>{

    /**
     * 增加权限
     * @param request
     * @return
     */
    //AccessCoreResponse addAccess(AccessCoreRequest request);

    /**
     * 修改权限
     * @param request
     * @return
     */
    //AccessCoreResponse updateAccess(AccessCoreRequest request);

    /**
     * 根据id获取权限
     * @param request
     * @return
     */
    //AccessQueryResponse getAccessById(AccessQueryRequest request);

    /**
     * 批量获取权限
     * @param request
     * @return
     */
    AccessQueryResponse getAccessList(AccessQueryRequest request);

    /**
     * 绑定权限菜单
     * @param request
     * @return
     */
    AccessMenuCoreResponse bindMenu(AccessMenuCoreRequest request);
}
