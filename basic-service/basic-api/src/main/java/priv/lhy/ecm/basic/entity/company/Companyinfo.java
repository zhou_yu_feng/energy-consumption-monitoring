package priv.lhy.ecm.basic.entity.company;


import java.io.Serializable;

public class Companyinfo implements Serializable {

    private static final long serialVersionUID = 1171362592159207881L;

    private long cid;
    private String cname;
    private long ctype;
    private double area;
    private double airarea;
    private String address;
    private double longitude;
    private double latitude;
    private String zipcode;
    private String aid;
    private String street;
    private long timezone;
    private String detailed;
    private String pic;
    private long status;
    private String svg;
    private long usernum;
    private String topological;
    private long etype;
    private String ctypeName;
    private String etypeName;

    public long getCid() {
        return cid;
    }

    public void setCid(long cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public long getCtype() {
        return ctype;
    }

    public void setCtype(long ctype) {
        this.ctype = ctype;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getAirarea() {
        return airarea;
    }

    public void setAirarea(double airarea) {
        this.airarea = airarea;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public long getTimezone() {
        return timezone;
    }

    public void setTimezone(long timezone) {
        this.timezone = timezone;
    }

    public String getDetailed() {
        return detailed;
    }

    public void setDetailed(String detailed) {
        this.detailed = detailed;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }


    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    public String getSvg() {
        return svg;
    }

    public void setSvg(String svg) {
        this.svg = svg;
    }

    public long getUsernum() {
        return usernum;
    }

    public void setUsernum(long usernum) {
        this.usernum = usernum;
    }

    public String getTopological() {
        return topological;
    }

    public void setTopological(String topological) {
        this.topological = topological;
    }

    public long getEtype() {
        return etype;
    }

    public void setEtype(long etype) {
        this.etype = etype;
    }

    public String getCtypeName() {
        return ctypeName;
    }

    public void setCtypeName(String ctypeName) {
        this.ctypeName = ctypeName;
    }

    public String getEtypeName() {
        return etypeName;
    }

    public void setEtypeName(String etypeName) {
        this.etypeName = etypeName;
    }

    @Override
    public String toString() {
        return "Companyinfo{" +
                "cid=" + cid +
                ", cname='" + cname + '\'' +
                ", ctype=" + ctype +
                ", area=" + area +
                ", airarea=" + airarea +
                ", address='" + address + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", zipcode='" + zipcode + '\'' +
                ", aid='" + aid + '\'' +
                ", street='" + street + '\'' +
                ", timezone=" + timezone +
                ", detailed='" + detailed + '\'' +
                ", pic='" + pic + '\'' +
                ", status=" + status +
                ", svg='" + svg + '\'' +
                ", usernum=" + usernum +
                ", topological='" + topological + '\'' +
                ", etype=" + etype +
                ", ctypeName='" + ctypeName + '\'' +
                ", etypeName='" + etypeName + '\'' +
                '}';
    }
}
