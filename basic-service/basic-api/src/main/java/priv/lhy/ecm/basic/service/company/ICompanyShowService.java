package priv.lhy.ecm.basic.service.company;

import org.springframework.transaction.annotation.Transactional;
import priv.lhy.ecm.basic.dto.companyShow.CompanyShowQueryRequest;
import priv.lhy.ecm.basic.dto.companyShow.CompanyShowQueryResponse;
import priv.lhy.ecm.basic.entity.company.CompanyShow;
import priv.lhy.ecm.basic.service.IService;

/**
 * @author: lihy
 * date: 2019/7/17 10:21
 * description:
 */
@Transactional
public interface ICompanyShowService extends IService<CompanyShow> {

    CompanyShowQueryResponse getCompanyShowList(CompanyShowQueryRequest request);
}
