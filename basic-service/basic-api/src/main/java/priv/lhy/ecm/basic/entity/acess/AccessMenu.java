package priv.lhy.ecm.basic.entity.acess;

import java.io.Serializable;

public class AccessMenu implements Serializable{

  private static final long serialVersionUID = 600426704934030447L;
  private Long amid;
  private Long aid;
  private Long mid;

  public Long getAmid() {
    return amid;
  }

  public void setAmid(Long amid) {
    this.amid = amid;
  }

  public Long getAid() {
    return aid;
  }

  public void setAid(Long aid) {
    this.aid = aid;
  }

  public Long getMid() {
    return mid;
  }

  public void setMid(Long mid) {
    this.mid = mid;
  }

  @Override
  public String toString() {
    return "AccessMenu{" +
            "amid=" + amid +
            ", aid=" + aid +
            ", mid=" + mid +
            '}';
  }
}
