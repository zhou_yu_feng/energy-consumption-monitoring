package priv.lhy.ecm.basic.dto.companyShow;

import priv.lhy.common.abs.AbstractRequest;

/**
 * @author: lihy
 * date: 2019/7/17 10:29
 * description:
 */
public class CompanyShowQueryRequest extends AbstractRequest {

    private Long cdid;
    private Long cid;
    private Integer status;
    private int pageNum;
    private int pageSize;

    public Long getCdid() {
        return cdid;
    }

    public void setCdid(Long cdid) {
        this.cdid = cdid;
    }

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
