package priv.lhy.ecm.basic.dto.access;

import priv.lhy.common.abs.AbstractRequest;

/**
 * author: lihy
 * date: 2019/5/6 12:57
 * description:
 */
public class AccessQueryRequest extends AbstractRequest {

    private long aid;

    private int pageNum;

    private int pageSize;

    public long getAid() {
        return aid;
    }

    public void setAid(long aid) {
        this.aid = aid;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
