package priv.lhy.ecm.basic.entity.company;

import java.io.Serializable;

public class CompanyShow implements Serializable {

    private static final long serialVersionUID = 3516350173435104766L;

    private Long cdid;

    private Long cid;

    private String dname;

    private String tableprex;

    private String colsname;

    private String colsfield;

    private Byte sort;

    private Byte status;

    private String unit;


    public Long getCdid() {
        return cdid;
    }

    public void setCdid(Long cdid) {
        this.cdid = cdid;
    }

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname == null ? null : dname.trim();
    }

    public String getTableprex() {
        return tableprex;
    }

    public void setTableprex(String tableprex) {
        this.tableprex = tableprex == null ? null : tableprex.trim();
    }

    public String getColsname() {
        return colsname;
    }

    public void setColsname(String colsname) {
        this.colsname = colsname == null ? null : colsname.trim();
    }

    public String getColsfield() {
        return colsfield;
    }

    public void setColsfield(String colsfield) {
        this.colsfield = colsfield == null ? null : colsfield.trim();
    }

    public Byte getSort() {
        return sort;
    }

    public void setSort(Byte sort) {
        this.sort = sort;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit == null ? null : unit.trim();
    }
}