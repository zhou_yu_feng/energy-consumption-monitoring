package priv.lhy.ecm.basic.dto.dict;

import priv.lhy.common.abs.AbstractRequest;
import priv.lhy.ecm.basic.entity.dict.Dictionaryinfo;
import priv.lhy.ecm.basic.entity.dict.Dictionarytype;

/**
 * author: lihy
 * date: 2019/4/10 15:16
 * description:
 */
public class DictCoreRequest extends AbstractRequest {

    private Dictionarytype type;
    private Dictionaryinfo info;

    public Dictionarytype getType() {
        return type;
    }

    public void setType(Dictionarytype type) {
        this.type = type;
    }

    public Dictionaryinfo getInfo() {
        return info;
    }

    public void setInfo(Dictionaryinfo info) {
        this.info = info;
    }
}
