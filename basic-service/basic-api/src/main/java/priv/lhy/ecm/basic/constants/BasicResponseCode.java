package priv.lhy.ecm.basic.constants;

/**
 * @author: lihy
 * date: 2019/4/10 14:46
 * description:
 */
public enum BasicResponseCode {
    SUCCESS("010000", "成功"),
    SYS_PARAM_NOT_RIGHT("010001", "请求参数不正确"),
    SYS_AUTHOR_ERROR("010002", "请求无权限"),
    SYS_RESUBMIT("010010", "请求已提交，请稍等"),
    SYSTEM_BUSY("010099", "系统繁忙，请稍后再试");

    private final String code;
    private final String msg;

    BasicResponseCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
