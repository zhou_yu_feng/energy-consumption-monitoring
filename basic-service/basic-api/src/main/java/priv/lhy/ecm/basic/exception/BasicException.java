package priv.lhy.ecm.basic.exception;

import priv.lhy.common.abs.AbstractRequest;
import priv.lhy.common.exception.AbstractException;

/**
 * author: lihy
 * date: 2019/4/10 14:39
 * description:
 */
public class BasicException extends AbstractException {
    public BasicException(String code, String msg, AbstractRequest request) {
        super(code, msg, request);
    }
}
