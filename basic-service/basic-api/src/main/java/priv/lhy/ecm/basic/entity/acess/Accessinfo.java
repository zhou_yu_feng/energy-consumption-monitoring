package priv.lhy.ecm.basic.entity.acess;


import java.io.Serializable;

public class Accessinfo implements Serializable{

  private static final long serialVersionUID = 1670599583439672975L;
  private long aid;
  private String aname;
  private long status;
  private String indexpage;


  public long getAid() {
    return aid;
  }

  public void setAid(long aid) {
    this.aid = aid;
  }


  public String getAname() {
    return aname;
  }

  public void setAname(String aname) {
    this.aname = aname;
  }


  public long getStatus() {
    return status;
  }

  public void setStatus(long status) {
    this.status = status;
  }


  public String getIndexpage() {
    return indexpage;
  }

  public void setIndexpage(String indexpage) {
    this.indexpage = indexpage;
  }

  @Override
  public String toString() {
    return "Accessinfo{" +
            "aid=" + aid +
            ", aname='" + aname + '\'' +
            ", status=" + status +
            ", indexpage='" + indexpage + '\'' +
            '}';
  }
}
