package priv.lhy.ecm.basic.service;

import priv.lhy.common.abs.AbstractCoreRequest;
import priv.lhy.common.abs.AbstractRequest;
import priv.lhy.common.abs.AbstractResponse;

/**
 * author: lihy
 * date: 2019/5/10 15:19
 * description:
 */
public interface IService<T> {

    AbstractResponse add(AbstractResponse response, AbstractCoreRequest request);

    AbstractResponse modify(AbstractResponse response, AbstractCoreRequest request);

    AbstractResponse add(AbstractResponse response, AbstractRequest request, T t);

    AbstractResponse modify(AbstractResponse response, AbstractRequest request, T t);

    AbstractResponse selectById(AbstractResponse response, AbstractRequest request, long id);
}
