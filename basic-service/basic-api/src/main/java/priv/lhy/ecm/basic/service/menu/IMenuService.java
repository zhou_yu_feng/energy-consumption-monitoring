package priv.lhy.ecm.basic.service.menu;

import org.springframework.transaction.annotation.Transactional;
import priv.lhy.ecm.basic.dto.menu.MenuCoreRequest;
import priv.lhy.ecm.basic.dto.menu.MenuCoreResponse;
import priv.lhy.ecm.basic.dto.menu.MenuQueryRequest;
import priv.lhy.ecm.basic.dto.menu.MenuQueryResponse;
import priv.lhy.ecm.basic.entity.menu.Menuinfo;
import priv.lhy.ecm.basic.service.IService;

/**
 * author: lihy
 * date: 2019/5/6 13:34
 * description:
 */
@Transactional
public interface IMenuService extends IService<Menuinfo>{


    /**
     * 增加菜单Entity
     * @param request
     * @return
     */
    //MenuCoreResponse addMenu(MenuCoreRequest request);

    /**
     * 修改菜单
     * @param request
     * @return
     */
    //MenuCoreResponse updateMenu(MenuCoreRequest request);

    /**
     * 根据id获取菜单
     * @param request
     * @return
     */
    //MenuQueryResponse getMenuById(MenuQueryRequest request);

    /**
     * 批量获取菜单
     * @param request
     * @return
     */
    MenuQueryResponse getMenuList(MenuQueryRequest request);

}
