package priv.lhy.ecm.basic.entity.dict;


import java.io.Serializable;

public class Dictionarytype implements Serializable {

  private static final long serialVersionUID = 3922753814335736319L;

  private long dtid;
  private String dtname;
  private long status;

  public long getDtid() {
    return dtid;
  }

  public void setDtid(long dtid) {
    this.dtid = dtid;
  }

  public String getDtname() {
    return dtname;
  }

  public void setDtname(String dtname) {
    this.dtname = dtname;
  }

  public long getStatus() {
    return status;
  }

  public void setStatus(long status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "Dictionarytype{" +
            "dtid=" + dtid +
            ", dtname='" + dtname + '\'' +
            ", status=" + status +
            '}';
  }
}
