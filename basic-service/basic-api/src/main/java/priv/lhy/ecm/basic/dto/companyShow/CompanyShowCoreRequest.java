package priv.lhy.ecm.basic.dto.companyShow;

import priv.lhy.common.abs.AbstractCoreRequest;
import priv.lhy.ecm.basic.entity.company.CompanyShow;

/**
 * @author: lihy
 * date: 2019/7/17 10:09
 * description:
 */
public class CompanyShowCoreRequest extends AbstractCoreRequest<CompanyShow> {
}
