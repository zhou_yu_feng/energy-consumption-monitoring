package priv.lhy.ecm.basic.service.dict;


import org.springframework.transaction.annotation.Transactional;
import priv.lhy.ecm.basic.dto.dict.DictCoreRequest;
import priv.lhy.ecm.basic.dto.dict.DictCoreResponse;
import priv.lhy.ecm.basic.dto.dict.DictQueryRequest;
import priv.lhy.ecm.basic.dto.dict.DictQueryResponse;

/**
 * author: lihy
 * date: 2019/4/10 16:20
 * description:
 */
@Transactional
public interface IDictionaryinfoService {

    DictCoreResponse addDictionaryinfo(DictCoreRequest request);

    DictQueryResponse getDictionaryinfo(DictQueryRequest request);

    DictQueryResponse getDictionaryinfosByType(DictQueryRequest request);


}
