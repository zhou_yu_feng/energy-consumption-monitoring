package priv.lhy.ecm.basic.dto.menu;

import priv.lhy.common.abs.AbstractCoreRequest;
import priv.lhy.ecm.basic.entity.menu.Menuinfo;

/**
 * author: lihy
 * date: 2019/5/6 13:08
 * description:
 */
public class MenuCoreRequest extends AbstractCoreRequest<Menuinfo> {


}
