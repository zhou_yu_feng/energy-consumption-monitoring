package priv.lhy.ecm.basic.dto.menu;

import org.omg.CORBA.INTERNAL;
import priv.lhy.common.abs.AbstractRequest;

/**
 * author: lihy
 * date: 2019/5/6 13:15
 * description:
 */
public class MenuQueryRequest extends AbstractRequest {

    private long mid;
    private String mname;
    private Integer mlevel;
    private Long parent;
    private Long mtype;
    private Integer status;
    private int pageNum;
    private int pageSize;

    public long getMid() {
        return mid;
    }

    public void setMid(long mid) {
        this.mid = mid;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public Integer getMlevel() {
        return mlevel;
    }

    public void setMlevel(Integer mlevel) {
        this.mlevel = mlevel;
    }

    public Long getParent() {
        return parent;
    }

    public void setParent(Long parent) {
        this.parent = parent;
    }

    public Long getMtype() {
        return mtype;
    }

    public void setMtype(Long mtype) {
        this.mtype = mtype;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
