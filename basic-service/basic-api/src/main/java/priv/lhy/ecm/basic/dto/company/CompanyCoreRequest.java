package priv.lhy.ecm.basic.dto.company;

import priv.lhy.common.abs.AbstractCoreRequest;
import priv.lhy.ecm.basic.entity.company.Companyinfo;

/**
 * author: lihy
 * date: 2019/5/10 14:02
 * description:
 */
public class CompanyCoreRequest extends AbstractCoreRequest<Companyinfo> {
    private long cid;

    public long getCid() {
        return cid;
    }

    public void setCid(long cid) {
        this.cid = cid;
    }
}
