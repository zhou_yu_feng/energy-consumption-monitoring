package priv.lhy.ecm.basic.entity.menu;

import java.io.Serializable;

public class Menuinfo implements Serializable{
  private static final long serialVersionUID = 1823583127514704895L;
  private Long mid;
  private String mname;
  private String nurl;
  private int mlevel;
  private String parent;
  private int sort;
  private int status;
  private Long mtype;
  private String ioc;

  public Long getMid() {
    return mid;
  }

  public void setMid(Long mid) {
    this.mid = mid;
  }

  public String getMname() {
    return mname;
  }

  public void setMname(String mname) {
    this.mname = mname;
  }

  public String getNurl() {
    return nurl;
  }

  public void setNurl(String nurl) {
    this.nurl = nurl;
  }

  public int getMlevel() {
    return mlevel;
  }

  public void setMlevel(int mlevel) {
    this.mlevel = mlevel;
  }

  public String getParent() {
    return parent;
  }

  public void setParent(String parent) {
    this.parent = parent;
  }

  public int getSort() {
    return sort;
  }

  public void setSort(int sort) {
    this.sort = sort;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public Long getMtype() {
    return mtype;
  }

  public void setMtype(Long mtype) {
    this.mtype = mtype;
  }

  public String getIoc() {
    return ioc;
  }

  public void setIoc(String ioc) {
    this.ioc = ioc;
  }

  @Override
  public String toString() {
    return "Menuinfo{" +
            "mid=" + mid +
            ", mname='" + mname + '\'' +
            ", nurl='" + nurl + '\'' +
            ", mlevel=" + mlevel +
            ", parent='" + parent + '\'' +
            ", sort=" + sort +
            ", status=" + status +
            ", mtype=" + mtype +
            ", ioc='" + ioc + '\'' +
            '}';
  }
}
