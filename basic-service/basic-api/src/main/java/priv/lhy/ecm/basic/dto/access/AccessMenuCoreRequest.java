package priv.lhy.ecm.basic.dto.access;

import priv.lhy.common.abs.AbstractRequest;
import priv.lhy.ecm.basic.entity.acess.AccessMenu;

import java.util.List;

/**
 * author: lihy
 * date: 2019/5/8 16:08
 * description:
 */
public class AccessMenuCoreRequest extends AbstractRequest {

    private long aid;
    private List<AccessMenu> accessMenus;

    public long getAid() {
        return aid;
    }

    public void setAid(long aid) {
        this.aid = aid;
    }

    public List<AccessMenu> getAccessMenus() {
        return accessMenus;
    }

    public void setAccessMenus(List<AccessMenu> accessMenus) {
        this.accessMenus = accessMenus;
    }
}
