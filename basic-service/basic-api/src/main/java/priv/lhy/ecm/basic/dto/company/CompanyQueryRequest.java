package priv.lhy.ecm.basic.dto.company;

import priv.lhy.common.abs.AbstractRequest;

/**
 * author: lihy
 * date: 2019/4/10 11:09
 * description:
 */
public class CompanyQueryRequest extends AbstractRequest {

    private Long cid;
    private String cname;
    private Long ctype;
    private String aid;
    private Integer status;
    private int pageNum;
    private int pageSize;

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public Long getCtype() {
        return ctype;
    }

    public void setCtype(Long ctype) {
        this.ctype = ctype;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
