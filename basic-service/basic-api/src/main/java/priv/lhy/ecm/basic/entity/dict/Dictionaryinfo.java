package priv.lhy.ecm.basic.entity.dict;


import java.io.Serializable;

public class Dictionaryinfo implements Serializable{
  private static final long serialVersionUID = 7820423873127671128L;

  private long did;
  private long dtid;
  private String dname;
  private long status;
  private long sort;


  public long getDid() {
    return did;
  }

  public void setDid(long did) {
    this.did = did;
  }

  public long getDtid() {
    return dtid;
  }

  public void setDtid(long dtid) {
    this.dtid = dtid;
  }

  public String getDname() {
    return dname;
  }

  public void setDname(String dname) {
    this.dname = dname;
  }

  public long getStatus() {
    return status;
  }

  public void setStatus(long status) {
    this.status = status;
  }

  public long getSort() {
    return sort;
  }

  public void setSort(long sort) {
    this.sort = sort;
  }

  @Override
  public String toString() {
    return "Dictionaryinfo{" +
            "did=" + did +
            ", dtid=" + dtid +
            ", dname='" + dname + '\'' +
            ", status=" + status +
            ", sort=" + sort +
            '}';
  }
}
