package priv.lhy.ecm.basic.service.company;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import priv.lhy.common.abs.AbstractCoreRequest;
import priv.lhy.common.abs.AbstractResponse;
import priv.lhy.common.constants.RedisConstant;
import priv.lhy.common.utils.MD5Util;
import priv.lhy.ecm.basic.constants.BasicResponseCode;
import priv.lhy.ecm.basic.dto.company.CompanyCoreRequest;
import priv.lhy.ecm.basic.dto.company.CompanyCoreResponse;
import priv.lhy.ecm.basic.dto.company.CompanyQueryRequest;
import priv.lhy.ecm.basic.dto.company.CompanyQueryResponse;
import priv.lhy.ecm.basic.entity.company.Companyinfo;
import priv.lhy.ecm.basic.exception.BasicException;
import priv.lhy.ecm.basic.mapper.company.CompanyinfoMapper;
import priv.lhy.ecm.basic.service.AbsService;
import priv.lhy.ecm.basic.util.RedisUtil;
import priv.lhy.ecm.basic.util.ResponseUtil;

import java.util.List;
import java.util.Map;

/**
 * author: lihy
 * date: 2019/5/10 15:00
 * description:
 */
@Service(interfaceClass = ICompanyService.class)
@Component
public class CompanyServiceImpl extends AbsService<Companyinfo> implements ICompanyService {

    private CompanyinfoMapper companyinfoMapper;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Value("${redis.data.index.company}")
    private int dataIndex;

    @Autowired
    public CompanyServiceImpl(CompanyinfoMapper mapper) {
        super(mapper);
        this.companyinfoMapper = mapper;
    }

    @Override
    public AbstractResponse add(AbstractResponse response, AbstractCoreRequest request) {
        AbstractResponse companyCoreResponse = super.add(response, request);
        if(companyCoreResponse.getCode().equals(BasicResponseCode.SUCCESS.getCode())){
            Companyinfo info = (Companyinfo) request.getT();
            //切换数据库
            redisTemplate = RedisUtil.switchDatabase(redisTemplate, dataIndex);
            //生成token
            String token = MD5Util.getMD5Str(info.getCname(), String.valueOf(info.getUsernum()));
            //将token放入COLLECTOR:TOKEN
            redisTemplate.opsForSet().add(RedisConstant.COLLECTOR_TOKEN, token);
        }
        return companyCoreResponse;
    }

    /**
     * 根据条件获取企业信息
     * @param request
     * @return
     */
    @Override
    public CompanyQueryResponse getCompanyList(CompanyQueryRequest request) {
        CompanyQueryResponse response = new CompanyQueryResponse();
        String sql = " 1=1 ";
        try {
            if (null != request.getCid()) {
                sql += String.format("cid=%d", request.getCid());
            }
            if (null != request.getCname()) {
                sql += String.format("cname=like '%s'", "%" + request.getCname() + "%");
            }
            if (null != request.getCtype()) {
                sql += String.format("ctype=%d", request.getCtype());
            }
            if (null != request.getAid()) {
                sql += String.format("Aid=%s", request.getAid());
            }
            if (null != request.getStatus()) {
                sql += String.format("status=%d", request.getStatus());
            }
            if (request.getPageSize() != 0) {
                PageHelper.startPage(request.getPageNum(), request.getPageSize());
            }
            Page<Companyinfo> list = companyinfoMapper.selectByCondition(sql);
            /*if (null != list && list.size() > 0) {
                ResponseUtil.getResponse(response, BasicResponseCode.SUCCESS);
                response.setData(list);
                response.setCount(list.getPages());
            } else {
                ResponseUtil.getResponse(response, BasicResponseCode.SYS_PARAM_NOT_RIGHT);
            }*/
            super.getResponse(list, response);
        } catch (Exception e) {
            throw new BasicException(BasicResponseCode.SYSTEM_BUSY.getCode(), BasicResponseCode
                    .SYSTEM_BUSY.getMsg(), request);
        }
        return response;
    }

    /**
     * 获取企业名称和userCode，用于生成企业上传token
     * @return
     */
    public List<Companyinfo> companyToken(){
        return companyinfoMapper.selectNameAndCode();
    }
}
