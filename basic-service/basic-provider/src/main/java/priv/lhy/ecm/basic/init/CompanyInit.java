package priv.lhy.ecm.basic.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import priv.lhy.common.constants.RedisConstant;
import priv.lhy.common.utils.MD5Util;
import priv.lhy.ecm.basic.entity.company.Companyinfo;
import priv.lhy.ecm.basic.service.company.CompanyServiceImpl;
import priv.lhy.ecm.basic.util.RedisUtil;

import java.util.List;
import java.util.Map;

/**
 * author: lihy
 * date: 2019/6/24 17:26
 * description: 初始化将企业上传数据token放入redis
 */
@Component
@Order(value = 1)
public class CompanyInit implements CommandLineRunner {

    @Autowired
    private CompanyServiceImpl service;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Value("${redis.data.index.company}")
    private int dataIndex;

    @Override
    public void run(String... args) throws Exception {
        //切换数据库
        redisTemplate = RedisUtil.switchDatabase(redisTemplate, 1);

        List<Companyinfo> nameAndCode = service.companyToken();
        String token;
        if (null == nameAndCode || nameAndCode.isEmpty()) return;
        for (Companyinfo info : nameAndCode) {
            //生成token
            token = MD5Util.getMD5Str(info.getCname(), String.valueOf(info.getUsernum()));
            //将token放入COLLECTOR:TOKEN
            redisTemplate.opsForSet().add(RedisConstant.COLLECTOR_TOKEN, token);
        }

    }
}
