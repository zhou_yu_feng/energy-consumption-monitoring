package priv.lhy.ecm.basic.mapper.dict;

import org.apache.ibatis.annotations.*;
import priv.lhy.ecm.basic.entity.dict.Dictionaryinfo;

import java.util.List;

/**
 * author: lihy
 * date: 2019/4/10 17:15
 * description:
 */
@Mapper
public interface DictionaryinfoMapper {

    //注释与xml配置只能选一个使用
    //@Insert("INSERT INTO em_dictionaryinfo VALUES(#{dtid}, #{dname}, #{status}, #{sort})")
    //@Options(useGeneratedKeys = true)
    int insert(Dictionaryinfo dict);

    //@Select("SELECT * FROM em_dictionaryinfo WHERE did = #{did}")
    Dictionaryinfo getDictById(long did);

    //@Select("SELECT * FROM em_dictionaryinfo WHERE 1=1 ${condition} ORDER BY sort ${sort}")
    List<Dictionaryinfo> getDicts(@Param("condition") String condition, @Param("sort") String sort);

}
