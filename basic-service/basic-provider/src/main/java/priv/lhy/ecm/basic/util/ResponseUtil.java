package priv.lhy.ecm.basic.util;

import priv.lhy.common.abs.AbstractResponse;
import priv.lhy.ecm.basic.constants.BasicResponseCode;

/**
 * author: lihy
 * date: 2019/4/12 9:28
 * description: 响应信息工具,转换不同的response
 */
public class ResponseUtil<T extends AbstractResponse> {

    public static<T extends AbstractResponse> T getResponse(T response, BasicResponseCode brc) {
        response.setCode(brc.getCode());
        response.setMsg(brc.getMsg());
        return response;
    }
}
