package priv.lhy.ecm.basic.mapper.access;

import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Mapper;
import priv.lhy.ecm.basic.dto.access.AccessQueryRequest;
import priv.lhy.ecm.basic.entity.acess.Accessinfo;
import priv.lhy.ecm.basic.mapper.AbsMapper;

import java.util.List;

/**
 * author: lihy
 * date: 2019/5/6 13:41
 * description:
 */
@Mapper
public interface AccessMapper extends AbsMapper<Accessinfo> {

    Page<Accessinfo> findByPaging();

}
