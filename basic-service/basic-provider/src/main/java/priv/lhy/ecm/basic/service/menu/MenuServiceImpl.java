package priv.lhy.ecm.basic.service.menu;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import priv.lhy.ecm.basic.annotation.RequestLogger;
import priv.lhy.ecm.basic.constants.BasicResponseCode;
import priv.lhy.ecm.basic.dto.menu.MenuQueryRequest;
import priv.lhy.ecm.basic.dto.menu.MenuQueryResponse;
import priv.lhy.ecm.basic.entity.menu.Menuinfo;
import priv.lhy.ecm.basic.exception.BasicException;
import priv.lhy.ecm.basic.mapper.menu.MenuinfoMapper;
import priv.lhy.ecm.basic.service.AbsService;
import priv.lhy.ecm.basic.util.ResponseUtil;

import java.util.List;

/**
 * author: lihy
 * date: 2019/5/7 9:28
 * description:
 */
@Service(interfaceClass = IMenuService.class)
@Component
public class MenuServiceImpl extends AbsService<Menuinfo> implements IMenuService {

    private MenuinfoMapper menuinfoMapper;

    @Autowired
    public MenuServiceImpl(MenuinfoMapper mapper) {
        super(mapper);
        this.menuinfoMapper = mapper;
    }

    /**
     * 批量获取菜单
     *
     * @param request
     * @return
     */
    @RequestLogger
    @Override
    public MenuQueryResponse getMenuList(MenuQueryRequest request) {
        MenuQueryResponse response = new MenuQueryResponse();
        String sql = "";

        try {
            if (null != request.getMlevel()) {
                sql += String.format(" and mlevel=%d", request.getMlevel());
            }
            if (null != request.getParent()) {
                sql += String.format(" and parent=%d", request.getParent());
            }
            if (null != request.getStatus()) {
                sql += String.format(" and status=%d", request.getStatus());
            }
            if (null != request.getMtype()) {
                sql += String.format(" and mtype=%d", request.getMtype());
            }
            if (null != request.getMname()) {
                sql += String.format(" and mname like '%s'", "%" + request.getMname() + "%");
            }
            if (request.getPageSize() != 0) {
                PageHelper.startPage(request.getPageNum(), request.getPageSize());
            }
            List<Menuinfo> list = menuinfoMapper.selectByCondition(sql);
            if (null != list && list.size() > 0) {
                ResponseUtil.getResponse(response, BasicResponseCode.SUCCESS);
                response.setData(list);
            } else {
                ResponseUtil.getResponse(response, BasicResponseCode.SYS_PARAM_NOT_RIGHT);
            }
        } catch (Exception e) {
            throw new BasicException(BasicResponseCode.SYSTEM_BUSY.getCode(), BasicResponseCode
                    .SYSTEM_BUSY.getMsg(), request);
        }
        return response;
    }



}
