package priv.lhy.ecm.basic.mapper.menu;

import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import priv.lhy.ecm.basic.entity.menu.Menuinfo;
import priv.lhy.ecm.basic.mapper.AbsMapper;

import java.util.List;

@Mapper
public interface MenuinfoMapper extends AbsMapper<Menuinfo>{
    Page<Menuinfo> selectByCondition(@Param("condition") String condition);

}