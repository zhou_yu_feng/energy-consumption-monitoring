package priv.lhy.ecm.basic.mapper.company;

import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import priv.lhy.ecm.basic.entity.company.Companyinfo;
import priv.lhy.ecm.basic.mapper.AbsMapper;

import java.util.List;

@Mapper
public interface CompanyinfoMapper extends AbsMapper<Companyinfo> {

    Page<Companyinfo> selectByCondition(@Param("condition") String condition);

    List<Companyinfo> selectNameAndCode();
}