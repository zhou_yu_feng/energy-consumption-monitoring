package priv.lhy.ecm.basic.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author: lihy
 * date: 2019/5/7 16:10
 * description: 防止重复提交注解
 */
@Retention(RetentionPolicy.RUNTIME) //注解的保留策略
@Target({ElementType.METHOD})       //注解的作用目标
public @interface ReSubmit {

    /**
     * 是否已提交请求
     * @return
     */
    boolean isSub() default false;
}
