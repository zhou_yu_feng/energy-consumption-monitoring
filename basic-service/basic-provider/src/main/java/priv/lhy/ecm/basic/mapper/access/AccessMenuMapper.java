package priv.lhy.ecm.basic.mapper.access;

import org.apache.ibatis.annotations.Mapper;
import priv.lhy.ecm.basic.entity.acess.AccessMenu;

import java.util.List;

/**
 * author: lihy
 * date: 2019/5/8 15:49
 * description:
 */
@Mapper
public interface AccessMenuMapper {

    int batchInert(List<AccessMenu> list);

    int delByAid(long aid);
}
