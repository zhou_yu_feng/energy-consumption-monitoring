package priv.lhy.ecm.basic.service;

import com.github.pagehelper.Page;
import priv.lhy.common.abs.AbstractCoreRequest;
import priv.lhy.common.abs.AbstractRequest;
import priv.lhy.common.abs.AbstractResponse;
import priv.lhy.common.constants.AuthorPolicy;
import priv.lhy.common.constants.MethodCode;
import priv.lhy.ecm.basic.annotation.AuthorValidate;
import priv.lhy.ecm.basic.annotation.ReSubmit;
import priv.lhy.ecm.basic.annotation.RequestLogger;
import priv.lhy.ecm.basic.constants.BasicResponseCode;
import priv.lhy.ecm.basic.exception.BasicException;
import priv.lhy.ecm.basic.mapper.AbsMapper;
import priv.lhy.ecm.basic.util.ResponseUtil;

import java.util.List;

/**
 * author: lihy
 * date: 2019/5/7 10:00
 * description:服务抽象类，抽象出重复代码
 */
public abstract class AbsService<T> implements IService<T> {

    private AbsMapper<T> mapper;

    public AbsService(AbsMapper<T> mapper) {
        this.mapper = mapper;
    }


    /**
     * 保存单个实体
     *
     * @param response
     * @param request
     * @return
     */
    @RequestLogger
    @ReSubmit
    @Override
    public AbstractResponse add(AbstractResponse response,
                                AbstractCoreRequest request) {
        return add(response, request, (T) request.getT());
    }

    /**
     * 修改单个实体
     *
     * @param response
     * @param request
     * @return
     */
    @RequestLogger
    @ReSubmit
    @Override
    public AbstractResponse modify(AbstractResponse response,
                                   AbstractCoreRequest request) {
        return modify(response, request, (T) request.getT());
    }

    /**
     * 保存单个实体
     *
     * @param response
     * @param request
     * @param t
     * @return
     */
    @RequestLogger
    @ReSubmit
    @Override
    public AbstractResponse add(AbstractResponse response, AbstractRequest request, T t) {
        return doAddOrModify(response, request, t, MethodCode.INSERT);
    }

    /**
     * 修改单个实体
     *
     * @param response
     * @param request
     * @param t
     * @return
     */
    @RequestLogger
    @ReSubmit
    @Override
    public AbstractResponse modify(AbstractResponse response, AbstractRequest request, T t) {
        return doAddOrModify(response, request, t, MethodCode.MODIFY);
    }

    /**
     * 根据主键获取单个实体
     *
     * @param response
     * @param request
     * @param id
     * @return
     */
    @AuthorValidate({AuthorPolicy.USER, AuthorPolicy.ADMIN})
    @RequestLogger
    @Override
    public AbstractResponse selectById(AbstractResponse response, AbstractRequest request, long id) {
        try {
            T t = mapper.selectByPrimaryKey(id);
            if (null != t) {
                response.setData(t);
                ResponseUtil.getResponse(response, BasicResponseCode.SUCCESS);
            } else {
                ResponseUtil.getResponse(response, BasicResponseCode.SYS_PARAM_NOT_RIGHT);
            }
        } catch (Exception e) {
            throw new BasicException(BasicResponseCode.SYSTEM_BUSY.getCode(), BasicResponseCode
                    .SYSTEM_BUSY.getMsg(), request);
        }
        return response;
    }

    /**
     * 保存或修改单个实体
     *
     * @param response
     * @param request
     * @param t
     * @param code
     * @return
     */
    @AuthorValidate(AuthorPolicy.ADMIN)
    private AbstractResponse doAddOrModify(AbstractResponse response, AbstractRequest request, T
            t, MethodCode code) {
        try {
            int count;
            switch (code) {
                //保存
                case INSERT:
                    count = mapper.insert(t);
                    break;
                //修改
                case MODIFY:
                    count = mapper.updateByPrimaryKey(t);
                    break;
                default:
                    throw new BasicException(BasicResponseCode.SYSTEM_BUSY.getCode(), BasicResponseCode
                            .SYSTEM_BUSY.getMsg(), request);
            }
            if (count > 0) {
                ResponseUtil.getResponse(response, BasicResponseCode.SUCCESS);
            } else {
                ResponseUtil.getResponse(response, BasicResponseCode.SYS_PARAM_NOT_RIGHT);
            }
        } catch (Exception e) {
            throw new BasicException(BasicResponseCode.SYSTEM_BUSY.getCode(), BasicResponseCode
                    .SYSTEM_BUSY.getMsg(), request);
        }
        return response;
    }

    protected AbstractResponse getResponse(Object obj, AbstractResponse response) {
        if (null != obj) {
            if (obj instanceof Page) {
                response.setCount(((Page) obj).getPages());
            }
            ResponseUtil.getResponse(response, BasicResponseCode.SUCCESS);
            response.setData(obj);
        } else {
            ResponseUtil.getResponse(response, BasicResponseCode.SYS_PARAM_NOT_RIGHT);
        }
        return response;
    }
}
