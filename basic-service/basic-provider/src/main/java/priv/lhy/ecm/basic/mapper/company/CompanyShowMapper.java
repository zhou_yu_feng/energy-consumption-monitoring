package priv.lhy.ecm.basic.mapper.company;

import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import priv.lhy.ecm.basic.entity.company.CompanyShow;
import priv.lhy.ecm.basic.mapper.AbsMapper;

@Mapper
public interface CompanyShowMapper extends AbsMapper<CompanyShow> {

    /**
     *
     * @param condition
     * @return
     */
    Page<CompanyShow> selectByCondition(@Param("condition") String condition);
}