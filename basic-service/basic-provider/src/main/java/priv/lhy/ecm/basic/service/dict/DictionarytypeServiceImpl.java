package priv.lhy.ecm.basic.service.dict;

import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import priv.lhy.ecm.basic.constants.BasicResponseCode;
import priv.lhy.ecm.basic.dto.dict.DictCoreRequest;
import priv.lhy.ecm.basic.dto.dict.DictCoreResponse;
import priv.lhy.ecm.basic.dto.dict.DictQueryRequest;
import priv.lhy.ecm.basic.dto.dict.DictQueryResponse;
import priv.lhy.ecm.basic.entity.dict.Dictionarytype;
import priv.lhy.ecm.basic.exception.BasicException;
import priv.lhy.ecm.basic.mapper.dict.DictionaryTypeMapper;
import priv.lhy.ecm.basic.util.ResponseUtil;

/**
 * author: lihy
 * date: 2019/4/10 17:52
 * description:
 */
@Service(interfaceClass = IDictionaryTypeService.class)
@Component
public class DictionarytypeServiceImpl implements IDictionaryTypeService {

    @Autowired
    private DictionaryTypeMapper mapper;

    @Override
    public DictCoreResponse addDictionarytype(DictCoreRequest request) {
        DictCoreResponse response = new DictCoreResponse();
        try {
            long dtid = mapper.addDictionaryType(request.getType());
            //System.out.println("dtid=" + dtid+", type.dtid="+request.getType().getDtid());
            if (dtid > 0) {
                ResponseUtil.getResponse(response, BasicResponseCode.SUCCESS);
            } else {
                ResponseUtil.getResponse(response, BasicResponseCode.SYS_PARAM_NOT_RIGHT);
            }
        } catch (Exception e) {
            throw new BasicException(BasicResponseCode.SYSTEM_BUSY.getCode(), BasicResponseCode.SYSTEM_BUSY.getMsg(), request);
        }
        return response;
    }

    @Override
    public DictQueryResponse getDictionarytype(DictQueryRequest request) {
        DictQueryResponse response = new DictQueryResponse();
        try {
            Dictionarytype type = mapper.getDictTypeById(request.getDtid());
            if (null != type) {
                ResponseUtil.getResponse(response, BasicResponseCode.SUCCESS);
            } else {
                ResponseUtil.getResponse(response, BasicResponseCode.SYS_PARAM_NOT_RIGHT);
            }
        } catch (Exception e) {
            throw new BasicException(BasicResponseCode.SYSTEM_BUSY.getCode(), BasicResponseCode.SYSTEM_BUSY.getMsg(), request);
        }
        return response;
    }
}
