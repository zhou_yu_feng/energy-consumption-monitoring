package priv.lhy.ecm.basic.annotation;

import priv.lhy.common.constants.AuthorPolicy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author: lihy
 * date: 2019/7/22 17:50
 * description:
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AuthorValidate {
    AuthorPolicy[] value() default AuthorPolicy.USER;
}
