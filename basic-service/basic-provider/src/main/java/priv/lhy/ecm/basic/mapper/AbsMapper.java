package priv.lhy.ecm.basic.mapper;


/**
 * author: lihy
 * date: 2019/5/7 10:03
 * description:通用Mapper方法
 */
public interface AbsMapper<T> {

    int deleteByPrimaryKey(Long id);

    int insert(T t);

    int insertSelective(T t);

    T selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(T t);

    int updateByPrimaryKey(T t);
}
