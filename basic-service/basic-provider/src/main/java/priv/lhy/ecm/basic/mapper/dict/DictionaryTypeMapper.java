package priv.lhy.ecm.basic.mapper.dict;

import org.apache.ibatis.annotations.*;
import priv.lhy.ecm.basic.entity.dict.Dictionarytype;

import java.util.List;

/**
 * author: lihy
 * date: 2019/4/10 17:15
 * description:
 */
@Mapper
public interface DictionaryTypeMapper {

    @Insert("INSERT INTO em_dictionarytype(dtname, status) VALUES(#{dtname}, #{status})")
    @Options(useGeneratedKeys = true, keyProperty = "dtid")
    long addDictionaryType(Dictionarytype dictType);

    @Select("SELECT * FROM em_dictionarytype where did = #{did}")
    Dictionarytype getDictTypeById(long did);

    @Select("SELECT * FROM em_dictionarytype where 1=1 ${condition}")
    List<Dictionarytype> dictTypes(@Param("condition") String condition);

    @Select("SELECT * FROM em_dictionarytype")
    List<Dictionarytype> allDictTypes();
}
