package priv.lhy.ecm.basic.service.company;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import priv.lhy.ecm.basic.constants.BasicResponseCode;
import priv.lhy.ecm.basic.dto.companyShow.CompanyShowQueryRequest;
import priv.lhy.ecm.basic.dto.companyShow.CompanyShowQueryResponse;
import priv.lhy.ecm.basic.entity.company.CompanyShow;
import priv.lhy.ecm.basic.exception.BasicException;
import priv.lhy.ecm.basic.mapper.company.CompanyShowMapper;
import priv.lhy.ecm.basic.service.AbsService;

/**
 * @author: lihy
 * date: 2019/7/17 10:23
 * description:
 */
@Service(interfaceClass = ICompanyShowService.class)
@Component
public class CompanyShowServiceImpl extends AbsService<CompanyShow> implements ICompanyShowService {

    private CompanyShowMapper companyShowMapper;

    @Autowired
    public CompanyShowServiceImpl(CompanyShowMapper mapper) {
        super(mapper);
        this.companyShowMapper = mapper;
    }


    @Override
    public CompanyShowQueryResponse getCompanyShowList(CompanyShowQueryRequest request) {
        CompanyShowQueryResponse response = new CompanyShowQueryResponse();
        String sql = " 1=1 ";
        try {
            if(null != request.getCdid()){
                sql += String.format("cdid=%d", request.getCdid());
            }
            if(null != request.getCid()){
                sql += String.format("cid=%d", request.getCid());
            }
            if (null != request.getStatus()) {
                sql += String.format("status=%d", request.getStatus());
            }
            if (request.getPageSize() != 0) {
                PageHelper.startPage(request.getPageNum(), request.getPageSize());
            }
            Page<CompanyShow> list = companyShowMapper.selectByCondition(sql);
            /*if (null != list && list.size() > 0) {
                ResponseUtil.getResponse(response, BasicResponseCode.SUCCESS);
                response.setData(list);
                response.setCount(list.getPages());
            } else {
                ResponseUtil.getResponse(response, BasicResponseCode.SYS_PARAM_NOT_RIGHT);
            }*/
            super.getResponse(list, response);
        } catch (Exception e) {
            throw new BasicException(BasicResponseCode.SYSTEM_BUSY.getCode(), BasicResponseCode
                    .SYSTEM_BUSY.getMsg(), request);
        }
        return response;
    }
}
