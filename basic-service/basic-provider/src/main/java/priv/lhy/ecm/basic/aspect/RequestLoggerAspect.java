package priv.lhy.ecm.basic.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import priv.lhy.common.abs.AbstractRequest;

import java.lang.reflect.Method;

/**
 * author: lihy
 * date: 2019/5/14 10:52
 * description: 打印请求日志AOP
 */
@Slf4j
@Aspect
@Component
public class RequestLoggerAspect {

    //private static final Logger LOGGER = LoggerFactory.getLogger(RequestLoggerAspect.class);

    @Pointcut("@annotation(priv.lhy.ecm.basic.annotation.RequestLogger)")
    public void requestLoggerPointcut() {
    }

    @Before("requestLoggerPointcut()")
    public void printRequest(JoinPoint point) {
        MethodSignature signature = (MethodSignature) point.getSignature();
        Method method = signature.getMethod();
        String methodName = method.getName();
        Object[] args = point.getArgs();
        AbstractRequest request = null;
        for (Object arg : args) {
            if (arg instanceof AbstractRequest) {
                request = (AbstractRequest) arg;
                break;
            }
        }
        log.info("=============================================================");
        log.info("request-method: " + methodName);
        log.info("request-seesionid: " + request.getSessionId());
        log.info("request-url: " + request.getUrl());
        log.info("request-time: " + request.getRequestTime());
        log.info("request-uid: " + request.getUid());
        log.info("=============================================================");
    }

}
