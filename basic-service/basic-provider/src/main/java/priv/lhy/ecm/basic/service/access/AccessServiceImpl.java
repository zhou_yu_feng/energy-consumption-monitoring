package priv.lhy.ecm.basic.service.access;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import priv.lhy.common.abs.AbstractRequest;
import priv.lhy.common.abs.AbstractResponse;
import priv.lhy.ecm.basic.annotation.ReSubmit;
import priv.lhy.ecm.basic.constants.BasicResponseCode;
import priv.lhy.ecm.basic.dto.access.*;
import priv.lhy.ecm.basic.entity.acess.Accessinfo;
import priv.lhy.ecm.basic.exception.BasicException;
import priv.lhy.ecm.basic.mapper.AbsMapper;
import priv.lhy.ecm.basic.mapper.access.AccessMapper;
import priv.lhy.ecm.basic.mapper.access.AccessMenuMapper;
import priv.lhy.ecm.basic.service.AbsService;
import priv.lhy.ecm.basic.util.ResponseUtil;

import java.util.List;

/**
 * author: lihy
 * date: 2019/5/6 15:14
 * description:
 */
@Service(interfaceClass = IAccessService.class)
@Component
public class AccessServiceImpl extends AbsService<Accessinfo> implements IAccessService {

    private AccessMapper accessMapper;

    @Autowired
    private AccessMenuMapper accessMenuMapper;

    @Autowired
    public AccessServiceImpl(AccessMapper mapper) {
        super(mapper);
        this.accessMapper = mapper;
    }

    /**
     * 批量获取权限信息
     * @param request
     * @return
     */
    @Override
    public AccessQueryResponse getAccessList(AccessQueryRequest request) {
        AccessQueryResponse response = new AccessQueryResponse();
        try {
            PageHelper.startPage(request.getPageNum(),request.getPageSize());
            Page<Accessinfo> list = accessMapper.findByPaging();
            ResponseUtil.getResponse(response, BasicResponseCode.SUCCESS);
            response.setData(list);
        } catch (Exception e) {
            throw new BasicException(BasicResponseCode.SYSTEM_BUSY.getCode(), BasicResponseCode.SYSTEM_BUSY
                    .getMsg(), request);
        }
        return response;
    }

    /**
     * 绑定权限菜单
     * @param request
     * @return
     */
    @ReSubmit
    @Override
    public AccessMenuCoreResponse bindMenu(AccessMenuCoreRequest request) {
        AccessMenuCoreResponse response = new AccessMenuCoreResponse();
        try {
            //先删除原绑定数据
            accessMenuMapper.delByAid(request.getAid());
            //新增绑定数据
            accessMenuMapper.batchInert(request.getAccessMenus());
            ResponseUtil.getResponse(response, BasicResponseCode.SUCCESS);
        } catch (Exception e) {
            throw new BasicException(BasicResponseCode.SYSTEM_BUSY.getCode(), BasicResponseCode.SYSTEM_BUSY
                    .getMsg(), request);
        }
        return response;
    }


}
