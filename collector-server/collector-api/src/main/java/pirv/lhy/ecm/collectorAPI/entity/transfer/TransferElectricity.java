package pirv.lhy.ecm.collectorAPI.entity.transfer;


import pirv.lhy.ecm.collectorAPI.entity.TransferData;
import pirv.lhy.ecm.collectorAPI.entity.data.Electricity;

/**
 * author: lihy
 * date: 2019/6/21 12:22
 * description: 用来传输的格式对象，将其转化成json传输
 */
public class TransferElectricity extends TransferData<Electricity> {

}
