package pirv.lhy.ecm.collectorAPI.constants;

/**
 * @author: lihy
 * date: 2019/6/29 16:14
 * description:
 */
public enum UploadType {
    ELECTRICITY, WATERMETER, GAS;
}
