package pirv.lhy.ecm.collectorAPI.constants;

import pirv.lhy.ecm.collectorAPI.entity.data.Electricity;
import pirv.lhy.ecm.collectorAPI.entity.data.Watermeter;

import java.util.*;

/**
 * author: lihy
 * date: 2019/6/26 16:11
 * description:
 */
public class TableInfo {

    //基础表前缀
    public static Map<Class, String> tableNamePrefix = new HashMap<>();
    //已创建的表
    public static Set<String> existTable = new HashSet<>();

    static {
        tableNamePrefix.put(Electricity.class, "em_electricity");
        tableNamePrefix.put(Watermeter.class, "em_watermeter");
    }


}
