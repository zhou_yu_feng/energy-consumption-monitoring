package pirv.lhy.ecm.collectorAPI.entity.transfer;

import pirv.lhy.ecm.collectorAPI.entity.TransferData;
import pirv.lhy.ecm.collectorAPI.entity.data.Watermeter;

/**
 * @author: lihy
 * date: 2019/7/1 11:39
 * description:
 */
public class TransferWatermeter extends TransferData<Watermeter> {
}
