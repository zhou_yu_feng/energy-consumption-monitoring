package pirv.lhy.ecm.collectorAPI.entity;

import java.io.Serializable;
import java.util.List;

/**
 * author: lihy
 * date: 2019/6/26 16:00
 * description:
 */
public class TransferData<T extends AbsCollectorData> implements Serializable {

    private static final long serialVersionUID = 2837925050416314832L;


    private List<T> list;


    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

}
