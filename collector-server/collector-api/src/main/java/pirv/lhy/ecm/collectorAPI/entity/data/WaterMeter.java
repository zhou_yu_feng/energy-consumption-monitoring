package pirv.lhy.ecm.collectorAPI.entity.data;

import pirv.lhy.ecm.collectorAPI.annotation.TableName;
import pirv.lhy.ecm.collectorAPI.entity.AbsCollectorData;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@TableName(name = "em_watermater")
public class Watermeter extends AbsCollectorData implements Serializable {
    private Long id;

    private Long eid;

    private BigDecimal flow;

    private BigDecimal dosage;

    private BigDecimal press;

    private BigDecimal temperature;

    private Date collecttime;

    private Integer nodenum;

    private Integer usernum;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEid() {
        return eid;
    }

    public void setEid(Long eid) {
        this.eid = eid;
    }

    public BigDecimal getFlow() {
        return flow;
    }

    public void setFlow(BigDecimal flow) {
        this.flow = flow;
    }

    public BigDecimal getDosage() {
        return dosage;
    }

    public void setDosage(BigDecimal dosage) {
        this.dosage = dosage;
    }

    public BigDecimal getPress() {
        return press;
    }

    public void setPress(BigDecimal press) {
        this.press = press;
    }

    public BigDecimal getTemperature() {
        return temperature;
    }

    public void setTemperature(BigDecimal temperature) {
        this.temperature = temperature;
    }

    public Date getCollecttime() {
        return collecttime;
    }

    public void setCollecttime(Date collecttime) {
        this.collecttime = collecttime;
    }

    public Integer getNodenum() {
        return nodenum;
    }

    public void setNodenum(Integer nodenum) {
        this.nodenum = nodenum;
    }

    public Integer getUsernum() {
        return usernum;
    }

    public void setUsernum(Integer usernum) {
        this.usernum = usernum;
    }
}