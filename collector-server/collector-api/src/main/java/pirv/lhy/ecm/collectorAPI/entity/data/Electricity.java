package pirv.lhy.ecm.collectorAPI.entity.data;

import pirv.lhy.ecm.collectorAPI.annotation.TableName;
import pirv.lhy.ecm.collectorAPI.entity.AbsCollectorData;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@TableName(name = "em_electricity")
public class Electricity extends AbsCollectorData implements Serializable {

    private static final long serialVersionUID = -1289280907820612961L;

    private Long esid;

    private Long eid;

    private BigDecimal total;

    private BigDecimal voltagea;

    private BigDecimal voltageb;

    private BigDecimal voltagec;

    private BigDecimal cvoltagea;

    private BigDecimal cvoltageb;

    private BigDecimal cvoltagec;

    private BigDecimal currenta;

    private BigDecimal currentb;

    private BigDecimal currentc;

    private BigDecimal powera;

    private BigDecimal powerb;

    private BigDecimal powerc;

    private BigDecimal repowera;

    private BigDecimal repowerb;

    private BigDecimal repowerc;

    private BigDecimal factora;

    private BigDecimal factorb;

    private BigDecimal factorc;

    private BigDecimal energy;

    private BigDecimal reenergy;

    private BigDecimal factor;

    private BigDecimal comreactivepower;

    private BigDecimal activepower;

    private BigDecimal reactivepower;

    private BigDecimal frequency;

    private Date collecttime;

    private BigDecimal consumption;

    private Integer nodenum;

    private Integer usernum;

    private BigDecimal intervalcons;

    public Long getEsid() {
        return esid;
    }

    public void setEsid(Long esid) {
        this.esid = esid;
    }

    public Long getEid() {
        return eid;
    }

    public void setEid(Long eid) {
        this.eid = eid;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public BigDecimal getVoltagea() {
        return voltagea;
    }

    public void setVoltagea(BigDecimal voltagea) {
        this.voltagea = voltagea;
    }

    public BigDecimal getVoltageb() {
        return voltageb;
    }

    public void setVoltageb(BigDecimal voltageb) {
        this.voltageb = voltageb;
    }

    public BigDecimal getVoltagec() {
        return voltagec;
    }

    public void setVoltagec(BigDecimal voltagec) {
        this.voltagec = voltagec;
    }

    public BigDecimal getCvoltagea() {
        return cvoltagea;
    }

    public void setCvoltagea(BigDecimal cvoltagea) {
        this.cvoltagea = cvoltagea;
    }

    public BigDecimal getCvoltageb() {
        return cvoltageb;
    }

    public void setCvoltageb(BigDecimal cvoltageb) {
        this.cvoltageb = cvoltageb;
    }

    public BigDecimal getCvoltagec() {
        return cvoltagec;
    }

    public void setCvoltagec(BigDecimal cvoltagec) {
        this.cvoltagec = cvoltagec;
    }

    public BigDecimal getCurrenta() {
        return currenta;
    }

    public void setCurrenta(BigDecimal currenta) {
        this.currenta = currenta;
    }

    public BigDecimal getCurrentb() {
        return currentb;
    }

    public void setCurrentb(BigDecimal currentb) {
        this.currentb = currentb;
    }

    public BigDecimal getCurrentc() {
        return currentc;
    }

    public void setCurrentc(BigDecimal currentc) {
        this.currentc = currentc;
    }

    public BigDecimal getPowera() {
        return powera;
    }

    public void setPowera(BigDecimal powera) {
        this.powera = powera;
    }

    public BigDecimal getPowerb() {
        return powerb;
    }

    public void setPowerb(BigDecimal powerb) {
        this.powerb = powerb;
    }

    public BigDecimal getPowerc() {
        return powerc;
    }

    public void setPowerc(BigDecimal powerc) {
        this.powerc = powerc;
    }

    public BigDecimal getRepowera() {
        return repowera;
    }

    public void setRepowera(BigDecimal repowera) {
        this.repowera = repowera;
    }

    public BigDecimal getRepowerb() {
        return repowerb;
    }

    public void setRepowerb(BigDecimal repowerb) {
        this.repowerb = repowerb;
    }

    public BigDecimal getRepowerc() {
        return repowerc;
    }

    public void setRepowerc(BigDecimal repowerc) {
        this.repowerc = repowerc;
    }

    public BigDecimal getFactora() {
        return factora;
    }

    public void setFactora(BigDecimal factora) {
        this.factora = factora;
    }

    public BigDecimal getFactorb() {
        return factorb;
    }

    public void setFactorb(BigDecimal factorb) {
        this.factorb = factorb;
    }

    public BigDecimal getFactorc() {
        return factorc;
    }

    public void setFactorc(BigDecimal factorc) {
        this.factorc = factorc;
    }

    public BigDecimal getEnergy() {
        return energy;
    }

    public void setEnergy(BigDecimal energy) {
        this.energy = energy;
    }

    public BigDecimal getReenergy() {
        return reenergy;
    }

    public void setReenergy(BigDecimal reenergy) {
        this.reenergy = reenergy;
    }

    public BigDecimal getFactor() {
        return factor;
    }

    public void setFactor(BigDecimal factor) {
        this.factor = factor;
    }

    public BigDecimal getComreactivepower() {
        return comreactivepower;
    }

    public void setComreactivepower(BigDecimal comreactivepower) {
        this.comreactivepower = comreactivepower;
    }

    public BigDecimal getActivepower() {
        return activepower;
    }

    public void setActivepower(BigDecimal activepower) {
        this.activepower = activepower;
    }

    public BigDecimal getReactivepower() {
        return reactivepower;
    }

    public void setReactivepower(BigDecimal reactivepower) {
        this.reactivepower = reactivepower;
    }

    public BigDecimal getFrequency() {
        return frequency;
    }

    public void setFrequency(BigDecimal frequency) {
        this.frequency = frequency;
    }

    public Date getCollecttime() {
        return collecttime;
    }

    public void setCollecttime(Date collecttime) {
        this.collecttime = collecttime;
    }

    public BigDecimal getConsumption() {
        return consumption;
    }

    public void setConsumption(BigDecimal consumption) {
        this.consumption = consumption;
    }

    public Integer getNodenum() {
        return nodenum;
    }

    public void setNodenum(Integer nodenum) {
        this.nodenum = nodenum;
    }

    public Integer getUsernum() {
        return usernum;
    }

    public void setUsernum(Integer usernum) {
        this.usernum = usernum;
    }

    public BigDecimal getIntervalcons() {
        return intervalcons;
    }

    public void setIntervalcons(BigDecimal intervalcons) {
        this.intervalcons = intervalcons;
    }
}