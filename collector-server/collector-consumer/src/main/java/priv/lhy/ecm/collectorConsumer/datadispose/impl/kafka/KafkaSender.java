package priv.lhy.ecm.collectorConsumer.datadispose.impl.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * @author: lihy
 * date: 2019/8/1 11:03
 * description:
 */
@Component
@ConditionalOnExpression("#{'2'.equals(environment['data.dispose.type'])}")
public class KafkaSender {

    @Value("${data.dispose.kafka.topic}")
    private String topic;

    @Autowired
    KafkaTemplate template;

    public void send(String message){
        template.send(topic, message);
    }
}
