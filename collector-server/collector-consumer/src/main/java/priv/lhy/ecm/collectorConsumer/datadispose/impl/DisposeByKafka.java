package priv.lhy.ecm.collectorConsumer.datadispose.impl;

import org.springframework.beans.factory.annotation.Autowired;
import priv.lhy.ecm.collectorConsumer.datadispose.AbstractDispose;
import priv.lhy.ecm.collectorConsumer.datadispose.impl.kafka.KafkaSender;

/**
 * @author: lihy
 * date: 2019/7/31 9:38
 * description:
 */
public class DisposeByKafka extends AbstractDispose {

    @Autowired
    private KafkaSender sender;

    @Override
    public String disposeLegalData(String[] message) {
        sender.send(message[1] + "|" + message[2] + "|" + message[3] + "|" + message[4]);
        return "ok";
    }
}
