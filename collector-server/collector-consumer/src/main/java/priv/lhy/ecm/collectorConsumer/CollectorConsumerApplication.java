package priv.lhy.ecm.collectorConsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CollectorConsumerApplication {


	public static void main(String[] args) {
		SpringApplication.run(CollectorConsumerApplication.class, args);
	}

}
