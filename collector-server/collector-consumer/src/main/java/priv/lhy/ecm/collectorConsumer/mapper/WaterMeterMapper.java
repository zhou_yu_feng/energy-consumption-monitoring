package priv.lhy.ecm.collectorConsumer.mapper;

import org.apache.ibatis.annotations.Mapper;
import pirv.lhy.ecm.collectorAPI.entity.data.Watermeter;

@Mapper
public interface WaterMeterMapper extends AbsMapper<Watermeter>{
}