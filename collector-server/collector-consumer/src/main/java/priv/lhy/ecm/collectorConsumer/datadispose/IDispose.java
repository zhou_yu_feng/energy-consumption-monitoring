package priv.lhy.ecm.collectorConsumer.datadispose;

/**
 * @author: lihy
 * date: 2019/7/29 15:45
 * description: 接收数据处理接口
 */
public interface IDispose {

    /**
     * 处理合法数据
     * @param message
     * @return
     */
    String disposeLegalData(String[] message);

    /**
     * 处理非法数据
     * @param ip
     * @return
     */
    String disposeIllegalData(String ip);
}
