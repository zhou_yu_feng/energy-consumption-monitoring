package priv.lhy.ecm.collectorConsumer.datadispose;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import priv.lhy.common.utils.StringUtil;
import priv.lhy.ecm.collectorConsumer.constants.DisposeType;
import priv.lhy.ecm.collectorConsumer.datadispose.impl.DisposeByDatabase;
import priv.lhy.ecm.collectorConsumer.datadispose.impl.DisposeByRabbitmq;

/**
 * @author: lihy
 * date: 2019/7/30 11:51
 * description:
 */
@Configuration
@Slf4j
public class DisposeConfig {

    @Value("${data.dispose.type}")
    private int type;

    private final String classPackage = "priv.lhy.ecm.collectorConsumer.datadispose.impl.";

    private final String classNamePrefix = "DisposeBy";

   /* @Bean
    @ConditionalOnExpression("#{'0'.equals(environment['data.dispose.type'])}")
    public IDispose databaseDispose(){
        log.info("使用database处理接收数据");
        return new DisposeByDatabase();
    }

    @Bean
    @ConditionalOnExpression("#{'1'.equals(environment['data.dispose.type'])}")
    public IDispose rabbitDispose(){
        log.info("使用rabbitMQ处理接收数据");
        return new DisposeByRabbitmq();
    }*/

    @Bean
    public IDispose getDispose() {
        String classNameSuffix = StringUtil.firstCharUpperCase(DisposeType.values()[type].name()
                .toLowerCase());
        log.info("使用" + classNameSuffix + "方式处理接收数据");
        try {
            Class<?> clazz = Class.forName(classPackage + classNamePrefix + classNameSuffix);
            return (IDispose) clazz.newInstance();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }

}
