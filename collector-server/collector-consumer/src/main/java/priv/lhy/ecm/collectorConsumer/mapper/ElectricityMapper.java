package priv.lhy.ecm.collectorConsumer.mapper;

import org.apache.ibatis.annotations.Mapper;
import pirv.lhy.ecm.collectorAPI.entity.data.Electricity;

@Mapper
public interface ElectricityMapper extends AbsMapper<Electricity>{

}