package priv.lhy.ecm.collectorConsumer.service.elec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pirv.lhy.ecm.collectorAPI.entity.data.Electricity;
import priv.lhy.ecm.collectorConsumer.mapper.ElectricityMapper;
import priv.lhy.ecm.collectorConsumer.service.AbsService;
import priv.lhy.ecm.collectorConsumer.service.IService;

import java.util.List;

/**
 * author: lihy
 * date: 2019/6/26 15:01
 * description:
 */
@Service("electricityService")
public class ElectricityServiceImpl extends AbsService<Electricity> implements IService<Electricity> {


    private ElectricityMapper elecMapper;

    public ElectricityServiceImpl(@Autowired ElectricityMapper mapper) {
        super(mapper);
        this.elecMapper = mapper;
    }

    @Override
    public int add(Electricity electricity) {
        super.createTable("");
        return elecMapper.insert(super.getTableName(), electricity);
    }

    @Override
    public int batchAdd(List<Electricity> list) {
        super.createTable("");
        return elecMapper.batchInsert(super.getTableName(), list);
    }

    @Override
    public int batchAdd(String tableSuffix, List<Electricity> list) {
        super.createTable(tableSuffix);
        return elecMapper.batchInsert(super.getTableName(), list);
    }
}
