package priv.lhy.ecm.collectorConsumer.datadispose.impl.rabbit;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author: lihy
 * date: 2019/7/31 10:54
 * description:
 */
@Component
@ConditionalOnExpression("#{'1'.equals(environment['data.dispose.type'])}")
public class RabbitSender {

    @Value("${data.dispose.rabbit.directexchange}")
    private String exchange;

    @Value("${data.dispose.rabbit.directroutingkey}")
    private String routingKey;

    @Resource
    private AmqpTemplate rabbitTemplate;

    public void sned(String message){
        rabbitTemplate.convertAndSend(exchange, routingKey, message);
    }

}
