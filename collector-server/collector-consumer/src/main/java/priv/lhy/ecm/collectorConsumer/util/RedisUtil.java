package priv.lhy.ecm.collectorConsumer.util;

import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * author: lihy
 * date: 2019/5/10 11:16
 * description:
 */
public class RedisUtil {

    /**
     * 切换数据库
     * @param redisTemplate
     * @param databaseIndex
     * @return
     */
    public static StringRedisTemplate switchDatabase(StringRedisTemplate redisTemplate, int
            databaseIndex){
        LettuceConnectionFactory factory = (LettuceConnectionFactory)redisTemplate.getConnectionFactory();
        factory.setShareNativeConnection(false);
        factory.setDatabase(databaseIndex);
        redisTemplate.setConnectionFactory(factory);
        return redisTemplate;
    }
}
