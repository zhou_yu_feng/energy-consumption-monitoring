package priv.lhy.ecm.collectorConsumer.datadispose.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import pirv.lhy.ecm.collectorAPI.entity.TransferData;
import priv.lhy.common.utils.JsonUtil;
import priv.lhy.common.utils.StringUtil;
import priv.lhy.ecm.collectorConsumer.datadispose.AbstractDispose;
import priv.lhy.ecm.collectorConsumer.service.IService;
import priv.lhy.ecm.collectorConsumer.service.elec.ElectricityServiceImpl;
import priv.lhy.ecm.collectorConsumer.service.waterMeter.WaterMeterServiceImpl;

import java.lang.reflect.Method;
import java.util.List;

/**
 * @author: lihy
 * date: 2019/7/29 16:03
 * description:
 */
public class DisposeByDatabase extends AbstractDispose {

    @Autowired
    private ApplicationContext applicationContext;

    /*@Autowired
    private ElectricityServiceImpl electricityService;

    @Autowired
    private WaterMeterServiceImpl waterMeterService;*/

    private final String transferPackage = "pirv.lhy.ecm.collectorAPI.entity.transfer";

    private final String entityPackage = "pirv.lhy.ecm.collectorAPI.entity.data";

    @Override
    public String disposeLegalData(String[] message) {
        /**
         * 上传数据格式：
         * token|userCode|uploadType|tableSuffix|jsonData|delimiter
         */
        try {
            //上传数据类型
            String uploadType = message[2].toLowerCase();
            //上传数据传输类
            Class transferClass = Class.forName(transferPackage + ".Transfer" + StringUtil
                    .firstCharUpperCase(uploadType));
            //上传数据实体类
            Class entityClass = Class.forName(entityPackage + "." + StringUtil.firstCharUpperCase
                    (uploadType));
            //将字符串转换为上传数据实例
            TransferData transferData = JsonUtil.json2Object(message[4], transferClass);
            //获取上传数据列表
            List list = transferData.getList();
            //获取对应数据处理service
            IService service = (IService) applicationContext.getBean(uploadType + "Service");
            //service父类注入companyCode
            Method setCompanyCodeMethod = service.getClass().getMethod("setCompanyCode", new Class<?>[]{java.lang.String.class});
            setCompanyCodeMethod.invoke(service, new Object[]{message[1]});
            //批量写入数据库
            service.batchAdd(message[3], list);


            /*switch (UploadType.valueOf(message[2])) {
                case ELECTRICITY:
                    TransferData transferElectricity = JsonUtil.json2Object(message[4],
                            TransferElectricity.class);
                    List<Electricity> electricities = transferElectricity.getList();
                    electricityService.setCompanyCode(message[1]);
                    electricityService.batchAdd(message[3], electricities);
                    break;
                case WATERMETER:
                    TransferData transferWaterMeter = JsonUtil.json2Object(message[4],
                            TransferWatermeter.class);
                    List<Watermeter> watermeters = transferWaterMeter.getList();
                    waterMeterService.setCompanyCode(message[1]);
                    waterMeterService.batchAdd(message[3], watermeters);
                default:
                    break;
            }*/
        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
        return "ok";
    }
}
