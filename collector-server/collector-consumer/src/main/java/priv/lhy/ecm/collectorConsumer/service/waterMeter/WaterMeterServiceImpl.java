package priv.lhy.ecm.collectorConsumer.service.waterMeter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pirv.lhy.ecm.collectorAPI.entity.data.Watermeter;
import priv.lhy.ecm.collectorConsumer.mapper.WaterMeterMapper;
import priv.lhy.ecm.collectorConsumer.service.AbsService;
import priv.lhy.ecm.collectorConsumer.service.IService;

import java.util.List;

/**
 * @author: lihy
 * date: 2019/7/1 12:09
 * description:
 */
@Service("watermeterService")
public class WaterMeterServiceImpl extends AbsService<Watermeter> implements IService<Watermeter> {

    private WaterMeterMapper waterMeterMapper;

    public WaterMeterServiceImpl(@Autowired WaterMeterMapper mapper) {
        super(mapper);
        this.waterMeterMapper = mapper;
    }

    @Override
    public int add(Watermeter watermeter) {
        super.createTable("");
        return waterMeterMapper.insert(super.getTableName(), watermeter);
    }

    @Override
    public int batchAdd(List<Watermeter> list) {
        super.createTable("");
        return waterMeterMapper.batchInsert(super.getTableName(), list);
    }

    @Override
    public int batchAdd(String tableSuffix, List<Watermeter> list) {
        super.createTable("");
        return waterMeterMapper.batchInsert(super.getTableName(), list);
    }
}
