package priv.lhy.ecm.collector.kafkaclient.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * @author: lihy
 * date: 2019/8/1 11:26
 * description:
 */
@Slf4j
@Component
public class DisposeConsumer {

    @KafkaHandler
    @KafkaListener(topics = "${data.dispose.kafka.topic}")
    public void process(String message){
        log.info(message);
    }
}
