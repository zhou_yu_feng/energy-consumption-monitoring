package priv.lhy.ecm.collectorProducer.aftertreatment.reupload;

/**
 * @author: lihy
 * date: 2019/7/3 10:04
 * description:
 */
public interface IReupload {

    void sendMsg();
}
