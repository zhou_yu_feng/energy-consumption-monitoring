package priv.lhy.ecm.collectorProducer.dal;

import java.io.Serializable;
import java.util.Date;

public class UploadDataBack implements Serializable {
    private Long id;

    private String uploadtime;

    private Integer exceptiontype;

    private Date collecttime;

    private String uploaddata;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUploadtime() {
        return uploadtime;
    }

    public void setUploadtime(String uploadtime) {
        this.uploadtime = uploadtime == null ? null : uploadtime.trim();
    }

    public Integer getExceptiontype() {
        return exceptiontype;
    }

    public void setExceptiontype(Integer exceptiontype) {
        this.exceptiontype = exceptiontype;
    }

    public Date getCollecttime() {
        return collecttime;
    }

    public void setCollecttime(Date collecttime) {
        this.collecttime = collecttime;
    }

    public String getUploaddata() {
        return uploaddata;
    }

    public void setUploaddata(String uploaddata) {
        this.uploaddata = uploaddata == null ? null : uploaddata.trim();
    }
}