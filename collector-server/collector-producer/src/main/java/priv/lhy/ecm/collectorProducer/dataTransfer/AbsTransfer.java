package priv.lhy.ecm.collectorProducer.dataTransfer;

import pirv.lhy.ecm.collectorAPI.entity.AbsCollectorData;
import pirv.lhy.ecm.collectorAPI.entity.TransferData;

/**
 * @author: lihy
 * date: 2019/6/24 15:35
 * description:
 */
public abstract class AbsTransfer<T extends AbsCollectorData> {


    public abstract TransferData<T> buildData();

}
