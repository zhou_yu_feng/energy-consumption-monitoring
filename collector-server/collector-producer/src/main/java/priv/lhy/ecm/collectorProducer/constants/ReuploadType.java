package priv.lhy.ecm.collectorProducer.constants;

/**
 * @author: lihy
 * date: 2019/7/3 10:16
 * description:
 */
public enum ReuploadType {
    FILE, DATABASE;
}
