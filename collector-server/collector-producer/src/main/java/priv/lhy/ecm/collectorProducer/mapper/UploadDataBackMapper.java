package priv.lhy.ecm.collectorProducer.mapper;

import org.apache.ibatis.annotations.Mapper;
import priv.lhy.ecm.collectorProducer.dal.UploadDataBack;

import java.util.List;

@Mapper
public interface UploadDataBackMapper {

    /**
     * 删除选中id的记录
     * @param list
     * @return
     */
    int deleteByPrimaryKeys(List<Long> list);

    /**
     * 新增记录
     * @param record
     * @return
     */
    int insert(UploadDataBack record);

    /**
     * 获取所有记录
     * @return
     */
    List<UploadDataBack> selectAll();

}