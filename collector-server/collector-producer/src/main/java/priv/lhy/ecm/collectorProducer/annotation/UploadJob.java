package priv.lhy.ecm.collectorProducer.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author: lihy
 * date: 2019/6/20 14:42
 * description: 上传任务注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface UploadJob {
    //任务名称
    String name();
    //任务分组
    String group();
    //任务计划
    String[] cron() default "";
}
