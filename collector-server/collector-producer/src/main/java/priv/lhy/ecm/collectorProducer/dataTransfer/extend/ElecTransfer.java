package priv.lhy.ecm.collectorProducer.dataTransfer.extend;

import org.springframework.stereotype.Component;
import pirv.lhy.ecm.collectorAPI.entity.transfer.TransferElectricity;
import pirv.lhy.ecm.collectorAPI.entity.data.Electricity;
import priv.lhy.ecm.collectorProducer.dataTransfer.AbsTransfer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * author: lihy
 * date: 2019/6/24 16:38
 * description:
 */
@Component
public class ElecTransfer extends AbsTransfer<Electricity> {


    @Override
    public TransferElectricity buildData() {
        TransferElectricity te = new TransferElectricity();
        Electricity elec = new Electricity();

        elec.setActivepower(BigDecimal.valueOf(1));
        elec.setComreactivepower(BigDecimal.valueOf(1));

        elec.setCurrenta(BigDecimal.valueOf(1));
        elec.setCurrentb(BigDecimal.valueOf(1));
        elec.setCurrentc(BigDecimal.valueOf(1));

        elec.setVoltagea(BigDecimal.valueOf(1));
        elec.setVoltageb(BigDecimal.valueOf(1));
        elec.setVoltagec(BigDecimal.valueOf(1));

        elec.setEnergy(BigDecimal.valueOf(1));

        elec.setFactor(BigDecimal.valueOf(1));
        elec.setFactora(BigDecimal.valueOf(1));
        elec.setFactorb(BigDecimal.valueOf(1));
        elec.setFactorc(BigDecimal.valueOf(1));

        elec.setPowera(BigDecimal.valueOf(1));
        elec.setPowerb(BigDecimal.valueOf(1));
        elec.setPowerc(BigDecimal.valueOf(1));

        elec.setReactivepower(BigDecimal.valueOf(1));
        elec.setRepowera(BigDecimal.valueOf(1));
        elec.setRepowerb(BigDecimal.valueOf(1));
        elec.setRepowerc(BigDecimal.valueOf(1));

        elec.setReenergy(BigDecimal.valueOf(1));

        elec.setTotal(BigDecimal.valueOf(1));

        elec.setFrequency(BigDecimal.valueOf(1));

        elec.setNodenum(11);
        elec.setUsernum(22);


        elec.setCollecttime(new Date());

        //电量
        //elec.setDl(1);


        List<Electricity> list = new ArrayList<>();
        list.add(elec);
        te.setList(list);

        return te;
    }
}
