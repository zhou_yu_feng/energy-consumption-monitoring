package priv.lhy.ecm.collectorProducer.task.job;

import io.netty.bootstrap.Bootstrap;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import pirv.lhy.ecm.collectorAPI.constants.UploadType;
import priv.lhy.ecm.collectorProducer.aftertreatment.record.IDispose;
import priv.lhy.ecm.collectorProducer.annotation.UploadJob;
import priv.lhy.ecm.collectorProducer.dataTransfer.extend.WaterMeterTransfer;

/**
 * @author: lihy
 * date: 2019/7/1 12:05
 * description:
 */
@UploadJob(name = "waterMeterJob", group = "waterMeterGroup")
public class WaterMeterJob extends AbsJob implements Job {

    public WaterMeterJob(@Value("${netty.tcp.host}") String tcpHost,
                         @Value("${netty.tcp.port}") int tcpPort,
                         @Value("${netty.delimiter}") String delimiter,
                         @Value("${job.upload.validate.company}") String company,
                         @Value("${job.upload.validate.userCode}") String userCode,
                         @Autowired Bootstrap bootstrap,
                         @Autowired WaterMeterTransfer waterMeterTransfer) {
        super(tcpHost, tcpPort, delimiter, company, userCode, UploadType.WATERMETER, bootstrap,
                waterMeterTransfer);
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        super.execute(jobExecutionContext);
    }
}
