package priv.lhy.ecm.collectorProducer.aftertreatment.record.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import priv.lhy.common.utils.DateUtil;
import priv.lhy.ecm.collectorProducer.aftertreatment.record.IDispose;
import priv.lhy.ecm.collectorProducer.constants.ExceptionType;
import priv.lhy.ecm.collectorProducer.dal.UploadDataBack;
import priv.lhy.ecm.collectorProducer.service.UploadDataBackServiceImpl;

import java.util.Date;

/**
 * @author: lihy
 * date: 2019/7/2 11:41
 * description: 将上传异常的数据存入数据库
 */
@Slf4j
@Component("disposeByDatabase")
public class DisposeByDatabase implements IDispose {

    @Autowired
    private UploadDataBackServiceImpl service;

    @Override
    public void dispose(String msg, String uploadTime, ExceptionType exceptionType) {
        UploadDataBack dataBack = new UploadDataBack();
        dataBack.setCollecttime(new Date());
        dataBack.setExceptiontype(exceptionType.ordinal());
        dataBack.setUploaddata(msg);
        if(null == uploadTime || uploadTime.equals("")){
            uploadTime = DateUtil.dateCustomStr(new Date(), "yyyyMM");
        }
        dataBack.setUploadtime(uploadTime);
        service.addRecord(dataBack);
    }
}
