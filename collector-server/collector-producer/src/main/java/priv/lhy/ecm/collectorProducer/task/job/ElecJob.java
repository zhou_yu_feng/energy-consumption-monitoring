package priv.lhy.ecm.collectorProducer.task.job;

import io.netty.bootstrap.Bootstrap;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import pirv.lhy.ecm.collectorAPI.constants.UploadType;
import priv.lhy.ecm.collectorProducer.aftertreatment.record.IDispose;
import priv.lhy.ecm.collectorProducer.annotation.UploadJob;
import priv.lhy.ecm.collectorProducer.dataTransfer.extend.ElecTransfer;

/**
 * author: lihy
 * date: 2019/6/18 15:25
 * description:
 */
@UploadJob(name = "elecJob", group = "elecGroup")
public class ElecJob extends AbsJob implements Job {

    public ElecJob(@Value("${netty.tcp.host}") String tcpHost,
                   @Value("${netty.tcp.port}") int tcpPort,
                   @Value("${netty.delimiter}")String delimiter,
                   @Value("${job.upload.validate.company}") String company,
                   @Value("${job.upload.validate.userCode}") String userCode,
                   @Autowired Bootstrap bootstrap,
                   @Autowired ElecTransfer elecTransfer) {
        super(tcpHost, tcpPort, delimiter, company, userCode, UploadType.ELECTRICITY, bootstrap, elecTransfer);

    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        super.execute(jobExecutionContext);
    }


}
