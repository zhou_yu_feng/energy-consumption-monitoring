package priv.lhy.ecm.collectorProducer.aftertreatment.record;

import priv.lhy.ecm.collectorProducer.constants.ExceptionType;

/**
 * @author: lihy
 * date: 2019/6/29 11:22
 * description:
 */
public abstract class AbsDispose implements IDispose {

    @Override
    public abstract void dispose(String msg, String arg, ExceptionType exceptionType);
}
