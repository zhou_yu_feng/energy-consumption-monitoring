package priv.lhy.ecm.collectorProducer.dataTransfer.extend;

import org.springframework.stereotype.Component;
import pirv.lhy.ecm.collectorAPI.entity.data.Watermeter;
import pirv.lhy.ecm.collectorAPI.entity.transfer.TransferWatermeter;
import priv.lhy.ecm.collectorProducer.dataTransfer.AbsTransfer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author: lihy
 * date: 2019/7/1 11:59
 * description:
 */
@Component
public class WaterMeterTransfer extends AbsTransfer<Watermeter> {

    @Override
    public TransferWatermeter buildData() {
        TransferWatermeter twm = new TransferWatermeter();
        Watermeter wm = new Watermeter();
        wm.setCollecttime(new Date());
        wm.setDosage(BigDecimal.valueOf(10));
        wm.setFlow(BigDecimal.valueOf(2.3));
        wm.setNodenum(1);
        wm.setPress(BigDecimal.valueOf(23.34));
        wm.setTemperature(BigDecimal.valueOf(14.32));
        wm.setUsernum(10001);
        List<Watermeter> list = new ArrayList<>();
        list.add(wm);
        twm.setList(list);

        return twm;
    }
}
