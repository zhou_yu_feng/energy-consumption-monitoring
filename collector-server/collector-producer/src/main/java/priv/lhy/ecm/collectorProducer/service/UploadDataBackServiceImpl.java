package priv.lhy.ecm.collectorProducer.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import priv.lhy.ecm.collectorProducer.dal.UploadDataBack;
import priv.lhy.ecm.collectorProducer.mapper.UploadDataBackMapper;

import java.util.List;

/**
 * @author: lihy
 * date: 2019/7/2 16:42
 * description:
 */
@Service
public class UploadDataBackServiceImpl {

    @Autowired
    private UploadDataBackMapper mapper;

    public List<UploadDataBack> uploadDataBacks() {
        return mapper.selectAll();
    }

    public int addRecord(UploadDataBack dataBack) {
        return mapper.insert(dataBack);
    }

    public int delRecordsByIds(List<Long> ids) {
        if (null != ids && ids.size() > 0) {
            return mapper.deleteByPrimaryKeys(ids);
        }else {
            return 0;
        }
    }
}
