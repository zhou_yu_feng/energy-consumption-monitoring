package priv.lhy.ecm.collectorProducer.aftertreatment.record;


import priv.lhy.ecm.collectorProducer.constants.ExceptionType;

/**
 * @author: lihy
 * date: 2019/6/29 11:02
 * description:
 */
public interface IDispose {

    /**
     * 将数据根据异常类型写入对应文件
     * @param msg
     * @param arg 实现方法根据需要传递不同参数
     * @param exceptionType
     */
    void dispose(String msg, String arg, ExceptionType exceptionType);

}
