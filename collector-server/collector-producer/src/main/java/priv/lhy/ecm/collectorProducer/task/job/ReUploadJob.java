package priv.lhy.ecm.collectorProducer.task.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import priv.lhy.common.utils.StringUtil;
import priv.lhy.ecm.collectorProducer.aftertreatment.record.IDispose;
import priv.lhy.ecm.collectorProducer.aftertreatment.reupload.IReupload;
import priv.lhy.ecm.collectorProducer.annotation.UploadJob;
import priv.lhy.ecm.collectorProducer.constants.ReuploadType;

/**
 * @author: lihy
 * date: 2019/7/1 15:08
 * description:
 */
@UploadJob(name = "reUploadJob", group = "reUploadGroup")
public class ReuploadJob implements Job {

    @Autowired
    ApplicationContext applicationContext;

    @Value("${upload.keepRecord.type}")
    private int keepRecordType;

    //private IReupload reupload;

    @Bean
    private IReupload getReupload(){
        return (IReupload) applicationContext.getBean("reuploadBy"
                + StringUtil.firstCharUpperCase(ReuploadType.values()[keepRecordType].name().toLowerCase()));
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        /*reupload = (IReupload) applicationContext.getBean("reuploadBy"
                + StringUtil.firstCharUpperCase(ReuploadType.values()[keepRecordType].name().toLowerCase()));*/
        getReupload().sendMsg();
    }


}
