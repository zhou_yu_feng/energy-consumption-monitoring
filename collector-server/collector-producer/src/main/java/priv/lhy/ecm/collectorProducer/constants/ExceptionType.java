package priv.lhy.ecm.collectorProducer.constants;

/**
 * @author: lihy
 * date: 2019/6/29 11:39
 * description:
 */
public enum ExceptionType {
    CONNECT_ERROR, ANALYSIS_ERROR, RETURN_ERROR,  CONNECT_EXCEPTION, MANUAL_ERROR;
}
