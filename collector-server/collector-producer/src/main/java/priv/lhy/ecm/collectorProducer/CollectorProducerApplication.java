package priv.lhy.ecm.collectorProducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CollectorProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CollectorProducerApplication.class, args);
	}

}
