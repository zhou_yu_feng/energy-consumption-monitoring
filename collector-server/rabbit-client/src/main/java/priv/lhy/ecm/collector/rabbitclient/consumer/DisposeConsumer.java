package priv.lhy.ecm.collector.rabbitclient.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

/**
 * @author: lihy
 * date: 2019/7/31 13:24
 * description:
 */
@Slf4j
@Component
public class DisposeConsumer {

    /**
     *  消息处理
     * @param message
     */
    //@RabbitListener需要放在方法级，不如会报找不到方法的错误
    @RabbitHandler
    @RabbitListener(queues = "${data.dispose.rabbit.queue}")
    public void process(@Payload String message){
        log.info(message);
    }
}
