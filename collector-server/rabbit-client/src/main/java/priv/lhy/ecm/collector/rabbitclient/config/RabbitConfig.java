package priv.lhy.ecm.collector.rabbitclient.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: lihy
 * date: 2019/7/31 13:11
 * description:
 */
@Configuration
public class RabbitConfig {

    @Value("${data.dispose.rabbit.queue}")
    private String queue;

    @Value("${data.dispose.rabbit.directexchange}")
    private String exchange;

    @Value("${data.dispose.rabbit.directroutingkey}")
    private String rountingKey;

    @Bean("disposeQueue")
    public Queue getDisposeQueue(){
        return new Queue(queue);
    }

    @Bean("disposeExchange")
    public DirectExchange getDirectExchange(){
        // 参数1：交换机名称
        // 参数2：是否持久化
        // 参数3：是否自动删除
        return new DirectExchange(exchange, true, true);
    }

    @Bean
    public Binding bind(@Qualifier("disposeQueue") Queue queue, @Qualifier("disposeExchange") DirectExchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with(rountingKey);
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rlcFactory(ConnectionFactory connectionFactory){
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(new SimpleMessageConverter());
        factory.setAcknowledgeMode(AcknowledgeMode.NONE);
        factory.setAutoStartup(true);
        return factory;
    }
}
