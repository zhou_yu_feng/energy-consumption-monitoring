package priv.lhy.ecm.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import priv.lhy.common.abs.AbstractRequest;
import priv.lhy.user.exception.UserException;

import java.util.HashMap;
import java.util.Map;

/**
 * author: lihy
 * date: 2019/4/4 9:40
 * description:
 */
@RestControllerAdvice
public class SsoExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(SsoExceptionHandler.class);

    @ExceptionHandler(value = Exception.class)
    public static Object handleException(Exception e) {
        LOG.error("msg: {}", e.getMessage());
        Map<String, Object> map = new HashMap<>();
        map.put("code", "999999");
        map.put("msg", e.getMessage());
        return map;
    }

    @ExceptionHandler(value = UserException.class)
    public static Object handleSsoException(UserException e) {
        LOG.error("url: {}, msg: {}", e.getRequest().getUrl(), e.getMsg());
        Map<String, Object> map = new HashMap<>();
        map.put("code", e.getCode());
        map.put("msg", e.getMsg());
        map.put("url", e.getRequest().getUrl());
        map.put("requestTime", e.getRequest().getRequestTime().toString());
        return map;
    }
}
