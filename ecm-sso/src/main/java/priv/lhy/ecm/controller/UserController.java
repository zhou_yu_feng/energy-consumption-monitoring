package priv.lhy.ecm.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import priv.lhy.common.constants.WebConstant;
import priv.lhy.user.constants.UserResponseCode;
import priv.lhy.user.dto.UserLoginRequest;
import priv.lhy.user.dto.UserLoginResponse;
import priv.lhy.user.service.IUserCoreService;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * author: lihy
 * date: 2019/4/3 17:39
 * description:
 */
@RestController
public class UserController {

    @Reference
    IUserCoreService userCoreService;


    @PostMapping("/login")
    public Map<String, Object> login(@RequestParam("username") String username, @RequestParam
            ("password") String password, HttpServletRequest request) {

        Map<String, Object> map = new HashMap<>();
        map.put("code", UserResponseCode.SYSTEM_BUSY.getCode());
        map.put("msg", UserResponseCode.SYSTEM_BUSY.getMsg());
        UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setUserName(username);
        userLoginRequest.setPassword(password);
        userLoginRequest.setUrl("/login");

        UserLoginResponse response = userCoreService.login(userLoginRequest);
        map.put("code", response.getCode());
        map.put("msg", response.getMsg());
        map.put("data", WebConstant.ECM_ANALYSIS_URL);
        request.getSession().setAttribute("uid", response.getUid());
        request.getSession().setAttribute("username", response.getUsername());
        request.getSession().setAttribute("accessId", response.getAccessId());

        return map;
    }
}
