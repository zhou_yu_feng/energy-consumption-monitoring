package priv.lhy.ecm.collectorProducer.task.job;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import priv.lhy.ecm.collectorProducer.annotation.UploadJob;

import java.util.Date;

/**
 * author: lihy
 * date: 2019/6/18 15:25
 * description:
 */
@UploadJob(name = "job1", group = "group1", cron = "*/5 * * * * ?")
public class MyJob1 implements Job {

    @Value("${netty.tcp.host}")
    private String tcpHost;

    @Value("${netty.tcp.port}")
    private int tcpPort;

    @Autowired
    private Bootstrap bootstrap;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            ChannelFuture future = bootstrap.connect(tcpHost, tcpPort).sync();

            Channel channel = future.channel();
            channel.writeAndFlush("hello world, " + new Date().toString() + "$$");

            future.channel().closeFuture().sync();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
