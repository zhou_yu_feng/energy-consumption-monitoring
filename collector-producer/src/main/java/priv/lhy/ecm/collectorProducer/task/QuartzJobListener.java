package priv.lhy.ecm.collectorProducer.task;

import lombok.extern.slf4j.Slf4j;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

/**
 * author: lihy
 * date: 2019/6/18 15:52
 * description:
 */
@Slf4j
@Configuration
public class QuartzJobListener implements ApplicationListener<ContextRefreshedEvent>{

    @Autowired
    private JobManager jobManager;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        try {
            jobManager.startJob();
            log.info("任务已启动");
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}
