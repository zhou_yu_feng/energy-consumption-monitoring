package priv.lhy.ecm.collectorProducer.nettyClient;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.concurrent.EventExecutorGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * author: lihy
 * date: 2019/6/18 14:36
 * description:
 */
@Component
@Qualifier("clientHandler")
@ChannelHandler.Sharable
public class ClientHandler extends SimpleChannelInboundHandler<String> {

    private final static Logger LOGGER = LoggerFactory.getLogger(ClientHandler.class);

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
            LOGGER.info("返回信息："+msg);
            ctx.channel().close();
    }
}
