package priv.lhy.ecm.collectorProducer.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * author: lihy
 * date: 2019/6/20 14:42
 * description:
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface UploadJob {
    String name();
    String group();
    String cron();
}
