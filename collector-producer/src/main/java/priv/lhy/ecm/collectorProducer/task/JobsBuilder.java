package priv.lhy.ecm.collectorProducer.task;

import org.quartz.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternUtils;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import priv.lhy.ecm.collectorProducer.annotation.UploadJob;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.*;

/**
 * author: lihy
 * date: 2019/6/20 14:54
 * description: 创建job列表
 */
@Configuration
public class JobsBuilder implements ResourceLoaderAware {

    //spring容器
    private ResourceLoader resourceLoader;

    //job类所在包
    @Value("${job.package}")
    private String jobPackage;

    /**
     * 生成job列表
     * @return
     */
    @Bean("jobs")
    public Map<JobDetail, Set<? extends Trigger>> getJobs() {
        Map<JobDetail, Set<? extends Trigger>> jobs = new HashMap<>();
        try {
            List<Class> jobClassList = scanPackage(jobPackage);
            for (Class clazz : jobClassList) {
                String name;
                String group;
                String cron;

                for (Annotation anno : clazz.getDeclaredAnnotations()) {
                    if (((UploadJob) anno).name().equalsIgnoreCase("")) {
                        throw new RuntimeException("必须定义job名称");
                    }
                    if (((UploadJob) anno).group().equalsIgnoreCase("")) {
                        throw new RuntimeException("必须定义job组");
                    }
                    if (((UploadJob) anno).cron().equalsIgnoreCase("")) {
                        throw new RuntimeException("必须定义job计划");
                    }
                    name = ((UploadJob) anno).name();
                    group = ((UploadJob) anno).group();
                    cron = ((UploadJob) anno).cron();

                    JobDetail detail = JobBuilder.newJob(clazz).withIdentity(name, group).build();
                    CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule
                            (cron);
                    CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(name, group)
                            .withSchedule(cronScheduleBuilder).build();
                    Set<Trigger> triggerSet = new HashSet<>();
                    triggerSet.add(trigger);
                    jobs.put(detail, triggerSet);
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jobs;
    }

    /**
     * 获取job类
     * @param classPath
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private List<Class> scanPackage(String classPath) throws IOException, ClassNotFoundException {
        List<Class> list = new ArrayList<>();
        ResourcePatternResolver resolver = ResourcePatternUtils.getResourcePatternResolver(resourceLoader);
        MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourceLoader);
        Resource[] resources = resolver.getResources("classpath*:" + classPath.replace(".",
                "/") + "/*.class");
        for (Resource r : resources) {
            MetadataReader reader = metadataReaderFactory.getMetadataReader(r);
            Class clazz = Class.forName(reader.getClassMetadata().getClassName());
            //只选择有job注解的类
            if (clazz.isAnnotationPresent(UploadJob.class)) list.add(clazz);
        }
        return list;
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
}
