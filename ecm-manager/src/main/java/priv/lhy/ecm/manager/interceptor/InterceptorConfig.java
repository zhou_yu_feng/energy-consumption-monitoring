package priv.lhy.ecm.manager.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * author: lihy
 * date: 2019/5/14 10:26
 * description:
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //日志拦截器，拦截所有连接
        registry.addInterceptor(new LoggerInterceptor()).addPathPatterns("/**");
    }
}
