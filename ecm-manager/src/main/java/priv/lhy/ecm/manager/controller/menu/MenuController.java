package priv.lhy.ecm.manager.controller.menu;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import priv.lhy.ecm.basic.dto.menu.MenuQueryRequest;
import priv.lhy.ecm.basic.dto.menu.MenuQueryResponse;
import priv.lhy.ecm.basic.entity.menu.Menuinfo;
import priv.lhy.ecm.basic.service.IService;
import priv.lhy.ecm.basic.service.menu.IMenuService;
import priv.lhy.ecm.manager.controller.AbsController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * author: lihy
 * date: 2019/5/7 10:16
 * description:菜单controller
 */
@RestController
public class MenuController extends AbsController<Menuinfo> {

    @Reference
    private IMenuService menuService;

    @Override
    public IService getService() {
        return this.menuService;
    }

    /**
     * 增加菜单
     * @param menu
     * @param request
     * @return
     */
    @RequestMapping("/menu/addMenu")
    @Override
    public Map<String, Object> add(@RequestBody Menuinfo menu, HttpServletRequest request) {
        return super.add(menu, request);
    }

    /**
     * 修改菜单
     * @param menu
     * @param request
     * @return
     */
    @RequestMapping("/menu/updateMenu")
    @Override
    public Map<String, Object> modify(@RequestBody Menuinfo menu, HttpServletRequest request) {
        return super.modify(menu, request);
    }


    /**
     * 查询菜单信息
     * @param menuRequest
     * @param request
     * @return
     */
    @RequestMapping("/menu/menus")
    public Map<String, Object> getMenus(@RequestBody MenuQueryRequest menuRequest,
                                        HttpServletRequest request) {
        menuRequest = (MenuQueryRequest) super.assemblyRequest(menuRequest, request);
        MenuQueryResponse response = menuService.getMenuList(menuRequest);
        Map<String, Object> result = super.assemblyResult(response);
        result.put("list", response.getData());
        return result;
    }
}
