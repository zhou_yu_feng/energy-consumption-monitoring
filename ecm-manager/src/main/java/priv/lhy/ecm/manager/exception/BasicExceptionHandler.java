package priv.lhy.ecm.manager.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import priv.lhy.ecm.basic.exception.BasicException;

import java.util.HashMap;
import java.util.Map;

/**
 * author: lihy
 * date: 2019/5/7 9:06
 * description:
 */
@RestControllerAdvice
public class BasicExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public static Object handleException(Exception e){
        Map<String, Object> map = new HashMap<>();
        map.put("code", "999999");
        map.put("msg", e.getMessage());
        return map;
    }

    @ExceptionHandler(value = BasicException.class)
    public static Object handleBasicException(BasicException e){
        Map<String, Object> map = new HashMap<>();
        map.put("code", e.getCode());
        map.put("msg", e.getMsg());
        map.put("url", e.getRequest().getUrl());
        map.put("requestTime", e.getRequest().getRequestTime().toString());
        return map;
    }
}
