package priv.lhy.ecm.manager.controller.dict;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import priv.lhy.common.controller.AbsBasicController;
import priv.lhy.ecm.basic.dto.dict.DictCoreRequest;
import priv.lhy.ecm.basic.dto.dict.DictCoreResponse;
import priv.lhy.ecm.basic.dto.dict.DictQueryRequest;
import priv.lhy.ecm.basic.dto.dict.DictQueryResponse;
import priv.lhy.ecm.basic.entity.dict.Dictionaryinfo;
import priv.lhy.ecm.basic.entity.dict.Dictionarytype;
import priv.lhy.ecm.basic.service.dict.IDictionaryTypeService;
import priv.lhy.ecm.basic.service.dict.IDictionaryinfoService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * author: lihy
 * date: 2019/4/10 18:25
 * description:数据字典controller
 */
@RestController
public class DictController extends AbsBasicController {

    @Reference
    IDictionaryTypeService typeService;

    @Reference
    IDictionaryinfoService infoService;

    /**
     * 增加字典类型
     * @param type
     * @param servletRequest
     * @return
     */
    @PostMapping("/addDictType")
    public Map<String, Object> addDictType(@RequestBody Dictionarytype type, HttpServletRequest servletRequest){
        DictCoreRequest request = new DictCoreRequest();
        request.setType(type);
        request = (DictCoreRequest) super.assemblyRequest(request, servletRequest);

        DictCoreResponse response = typeService.addDictionarytype(request);
        Map<String, Object> result = super.assemblyResult(response);
        return result;
    }

    /**
     * 增加字典信息
     * @param info
     * @param servletRequest
     * @return
     */
    @PostMapping("/addDictInfo")
    public Map<String, Object> addDictInfo(@RequestBody Dictionaryinfo info, HttpServletRequest
            servletRequest){
        DictCoreRequest request = new DictCoreRequest();
        request.setInfo(info);
        request = (DictCoreRequest) super.assemblyRequest(request, servletRequest);

        DictCoreResponse response = infoService.addDictionaryinfo(request);
        Map<String, Object> result = super.assemblyResult(response);
        return result;
    }

    /**
     * 根据类型获取字典信息
     * @param dtid
     * @return
     */
    @GetMapping("/getDicts")
    public Map<String,Object> getDicts(long dtid, HttpServletRequest servletRequest){
        DictQueryRequest request = new DictQueryRequest();
        request.setDtid(dtid);
        request = (DictQueryRequest) super.assemblyRequest(request, servletRequest);

        DictQueryResponse response = infoService.getDictionaryinfosByType(request);
        Map<String, Object> result = super.assemblyResult(response);
        result.put("dicts", response.getData());
        return result;
    }
}
