package priv.lhy.ecm.manager.domain.vo;

/**
 * author: lihy
 * date: 2019/5/8 16:59
 * description:
 */
public class BindMenuVO {
    private long aid;
    private long[] mids;

    public long getAid() {
        return aid;
    }

    public void setAid(long aid) {
        this.aid = aid;
    }

    public long[] getMids() {
        return mids;
    }

    public void setMids(long[] mids) {
        this.mids = mids;
    }
}
