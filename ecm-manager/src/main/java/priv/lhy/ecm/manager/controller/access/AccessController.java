package priv.lhy.ecm.manager.controller.access;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import priv.lhy.ecm.basic.dto.access.*;
import priv.lhy.ecm.basic.entity.acess.AccessMenu;
import priv.lhy.ecm.basic.entity.acess.Accessinfo;
import priv.lhy.ecm.basic.service.IService;
import priv.lhy.ecm.basic.service.access.IAccessService;
import priv.lhy.ecm.manager.controller.AbsController;
import priv.lhy.ecm.manager.domain.vo.BindMenuVO;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * author: lihy
 * date: 2019/5/6 15:36
 * description:权限controller
 */
@RestController
public class AccessController extends AbsController<Accessinfo> {

    @Reference
    private IAccessService accessService;


    @Override
    public IService getService() {
        return this.accessService;
    }

    @RequestMapping("/access/addAccess")
    @Override
    public Map<String, Object> add(@RequestBody Accessinfo access, HttpServletRequest request) {
        return super.add(access, request);
    }

    @RequestMapping("/access/updateAccess")
    @Override
    public Map<String, Object> modify(@RequestBody Accessinfo access, HttpServletRequest request) {
        return super.modify(access, request);
    }

    @RequestMapping("/access/accesList")
    public Map<String, Object> accessList(HttpServletRequest request) {
        AccessQueryRequest accessRequest = new AccessQueryRequest();
        accessRequest = (AccessQueryRequest) super.assemblyRequest(accessRequest, request);
        accessRequest.setPageNum(0);
        accessRequest.setPageSize(2);

        AccessQueryResponse response = accessService.getAccessList(accessRequest);
        Map<String, Object> result = super.assemblyResult(response);
        result.put("accessList", response.getData());

        return result;
    }

    @RequestMapping("/access/bindMenu")
    public Map<String, Object> bindMenu(@RequestBody BindMenuVO vo, HttpServletRequest request) {
        AccessMenuCoreRequest amRequest = new AccessMenuCoreRequest();
        amRequest = (AccessMenuCoreRequest) super.assemblyRequest(amRequest, request);
        List<AccessMenu> accessMenus = new ArrayList<>();
        for (long mid : vo.getMids()) {
            AccessMenu accessMenu = new AccessMenu();
            accessMenu.setAid(vo.getAid());
            accessMenu.setMid(mid);
            accessMenus.add(accessMenu);
        }
        amRequest.setAccessMenus(accessMenus);
        amRequest.setAid(vo.getAid());
        AccessMenuCoreResponse response = accessService.bindMenu(amRequest);
        return super.assemblyResult(response);
    }

    @RequestMapping("/access/getAccess")
    public Map<String, Object> getAccessById(@RequestParam long id, HttpServletRequest request) {
        AccessQueryRequest accessRequest = new AccessQueryRequest();
        accessRequest = (AccessQueryRequest) super.assemblyRequest(accessRequest, request);
        accessRequest.setAid(id);
        AccessQueryResponse response = new AccessQueryResponse();
        response = (AccessQueryResponse)accessService.selectById(response, accessRequest, accessRequest.getAid());
        Map<String, Object> result = super.assemblyResult(response);
        result.put("access", response.getData());
        return result;
    }

}
