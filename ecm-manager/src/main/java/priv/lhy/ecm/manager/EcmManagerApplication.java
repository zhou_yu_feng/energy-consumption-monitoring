package priv.lhy.ecm.manager;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubboConfiguration
public class EcmManagerApplication {



	public static void main(String[] args) {
		SpringApplication.run(EcmManagerApplication.class, args);
	}

}
