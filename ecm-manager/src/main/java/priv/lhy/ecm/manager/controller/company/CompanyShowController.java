package priv.lhy.ecm.manager.controller.company;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import priv.lhy.ecm.basic.dto.companyShow.CompanyShowQueryRequest;
import priv.lhy.ecm.basic.dto.companyShow.CompanyShowQueryResponse;
import priv.lhy.ecm.basic.entity.company.CompanyShow;
import priv.lhy.ecm.basic.service.IService;
import priv.lhy.ecm.basic.service.company.ICompanyShowService;
import priv.lhy.ecm.manager.controller.AbsController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


/**
 * @author: lihy
 * date: 2019/7/17 11:25
 * description:
 */
@RestController
public class CompanyShowController extends AbsController<CompanyShow> {

    @Reference
    private ICompanyShowService service;

    @Override
    public IService getService() {
        return this.service;
    }

    @PostMapping("company/addCompanyShow")
    @Override
    public Map<String, Object> add(@RequestBody CompanyShow companyShow, HttpServletRequest
            request) {
        return super.add(companyShow, request);
    }

    @PostMapping("company/updateCompanyShow")
    @Override
    public Map<String, Object> modify(@RequestBody CompanyShow companyShow, HttpServletRequest
            request) {
        return super.modify(companyShow, request);
    }

    @GetMapping("company/companyShows")
    public Map<String, Object> getCompanyShows(CompanyShowQueryRequest companyShowRequest,
                                            HttpServletRequest request){
        companyShowRequest = (CompanyShowQueryRequest) super.assemblyRequest(companyShowRequest,
                request);
        CompanyShowQueryResponse response = service.getCompanyShowList(companyShowRequest);
        Map<String, Object> result = super.assemblyResult(response);
        result.put("list", response.getData());
        result.put("count", response.getCount());
        return result;
    }
}
