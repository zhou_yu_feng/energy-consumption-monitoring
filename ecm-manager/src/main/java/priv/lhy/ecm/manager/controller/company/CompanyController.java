package priv.lhy.ecm.manager.controller.company;

import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import priv.lhy.ecm.basic.dto.company.CompanyQueryRequest;
import priv.lhy.ecm.basic.dto.company.CompanyQueryResponse;
import priv.lhy.ecm.basic.entity.company.Companyinfo;
import priv.lhy.ecm.basic.service.IService;
import priv.lhy.ecm.basic.service.company.ICompanyService;
import priv.lhy.ecm.manager.controller.AbsController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * author: lihy
 * date: 2019/5/13 9:48
 * description:企业controller
 */
@RestController
public class CompanyController extends AbsController<Companyinfo> {

    @Reference
    private ICompanyService companyService;

    @Override
    public IService getService() {
        return this.companyService;
    }

    @PostMapping("company/addCompany")
    @Override
    public Map<String, Object> add(@RequestBody Companyinfo companyinfo, HttpServletRequest request) {
        return super.add(companyinfo, request);
    }

    @PostMapping("company/updateCompany")
    @Override
    public Map<String, Object> modify(@RequestBody  Companyinfo companyinfo, HttpServletRequest
            request) {
        return super.modify(companyinfo, request);
    }

    @GetMapping("company/companies")
    public Map<String, Object> getCompanies(CompanyQueryRequest companyRequest, HttpServletRequest
            request){
        companyRequest = (CompanyQueryRequest) super.assemblyRequest(companyRequest, request);
        CompanyQueryResponse response = companyService.getCompanyList(companyRequest);
        Map<String, Object> result = super.assemblyResult(response);
        result.put("list", response.getData());
        return result;
    }
}
