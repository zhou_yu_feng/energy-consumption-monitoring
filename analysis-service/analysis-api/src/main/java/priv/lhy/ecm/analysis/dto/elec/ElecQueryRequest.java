package priv.lhy.ecm.analysis.dto.elec;

import priv.lhy.common.abs.AbstractRequest;

/**
 * @author: lihy
 * date: 2019/7/4 9:59
 * description:
 */
public class ElecQueryRequest extends AbstractRequest {

    private int pageNum;

    private int pageSize;

    private String showColumns;

    private String begin;

    private String end;

    private int companyCode;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getShowColumns() {
        return showColumns;
    }

    public void setShowColumns(String showColumns) {
        this.showColumns = showColumns;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public int getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(int companyCode) {
        this.companyCode = companyCode;
    }
}
