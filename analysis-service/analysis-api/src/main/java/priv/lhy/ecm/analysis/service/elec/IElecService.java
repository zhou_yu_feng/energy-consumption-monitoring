package priv.lhy.ecm.analysis.service.elec;

import org.springframework.transaction.annotation.Transactional;
import priv.lhy.ecm.analysis.entity.elec.Electricity;
import priv.lhy.ecm.analysis.service.IService;

/**
 * @author: lihy
 * date: 2019/7/4 10:03
 * description:
 */
@Transactional
public interface IElecService extends IService<Electricity> {

}
