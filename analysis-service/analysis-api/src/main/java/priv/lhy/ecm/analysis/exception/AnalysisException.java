package priv.lhy.ecm.analysis.exception;

import priv.lhy.common.abs.AbstractRequest;
import priv.lhy.common.exception.AbstractException;

/**
 * @author: lihy
 * date: 2019/7/4 9:58
 * description:
 */
public class AnalysisException extends AbstractException {

    public AnalysisException(String code, String msg, AbstractRequest request) {
        super(code, msg, request);
    }
}
