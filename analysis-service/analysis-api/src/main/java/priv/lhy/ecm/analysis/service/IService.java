package priv.lhy.ecm.analysis.service;

import priv.lhy.common.abs.AbstractRequest;
import priv.lhy.common.abs.AbstractResponse;

/**
 * @author: lihy
 * date: 2019/7/4 10:00
 * description:
 */
public interface IService<T> {

    /**
     *
     * @param request
     * @return
     */
    AbstractResponse selectByTime(AbstractRequest request);
}
