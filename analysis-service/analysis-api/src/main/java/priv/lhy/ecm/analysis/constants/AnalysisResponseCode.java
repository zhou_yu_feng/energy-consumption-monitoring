package priv.lhy.ecm.analysis.constants;

/**
 * @author: lihy
 * date: 2019/7/4 9:54
 * description:
 */
public enum  AnalysisResponseCode {
    SUCCESS("080000", "成功"),
    SYS_PARAM_NOT_RIGHT("080001", "请求参数不正确"),
    NO_DATA("080002", "未获取到数据"),
    SYS_RESUBMIT("080010", "请求已提交，请稍等"),
    SYSTEM_BUSY("080099", "系统繁忙，请稍后再试");

    private final String code;
    private final String msg;

    AnalysisResponseCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
