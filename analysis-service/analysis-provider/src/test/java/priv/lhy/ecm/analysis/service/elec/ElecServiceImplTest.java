package priv.lhy.ecm.analysis.service.elec;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import priv.lhy.ecm.analysis.AnalysisApplication;
import priv.lhy.ecm.analysis.dto.elec.ElecQueryRequest;
import priv.lhy.ecm.analysis.dto.elec.ElecQueryResponse;
import priv.lhy.ecm.analysis.entity.elec.Electricity;

import java.util.List;


/**
 * @author: lihy
 * date: 2019/7/4 14:24
 * description:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes={AnalysisApplication.class})
public class ElecServiceImplTest {

    @Autowired
    IElecService service;

    @Test
    public void selectByTime(){

        ElecQueryRequest request = new ElecQueryRequest();
        request.setBegin("2018-12-1 12:00:00");
        request.setEnd("2019-7-1 12:00:00");
        request.setShowColumns("voltagea,voltageb,voltagec,collecttime");
        request.setCompanyCode(10001);
        ElecQueryResponse response = (ElecQueryResponse) service.selectByTime(request);
        List<Electricity> list = (List<Electricity>) response.getData();
        System.out.println(list.size());
        for (Electricity elec: list) {
            System.out.println(elec.toString());
        }
    }
}