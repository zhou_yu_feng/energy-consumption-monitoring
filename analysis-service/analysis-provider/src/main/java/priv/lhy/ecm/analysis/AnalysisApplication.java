package priv.lhy.ecm.analysis;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author: lihy
 * date: 2019/7/4 9:42
 * description:
 */
@SpringBootApplication
@EnableDubboConfiguration
public class AnalysisApplication {

    public static void main(String[] args) {
        SpringApplication.run(AnalysisApplication.class);
    }
}
