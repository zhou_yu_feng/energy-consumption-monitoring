package priv.lhy.ecm.analysis.util;

import priv.lhy.common.abs.AbstractResponse;
import priv.lhy.ecm.analysis.constants.AnalysisResponseCode;

/**
 * author: lihy
 * date: 2019/4/12 9:28
 * description: 响应信息工具,转换不同的response
 */
public class ResponseUtil<T extends AbstractResponse> {

    public static<T extends AbstractResponse> T getResponse(T response, AnalysisResponseCode brc) {
        response.setCode(brc.getCode());
        response.setMsg(brc.getMsg());
        return response;
    }
}
