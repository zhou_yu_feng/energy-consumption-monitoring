package priv.lhy.ecm.analysis.mapper.elec;

import org.apache.ibatis.annotations.Mapper;
import priv.lhy.ecm.analysis.entity.elec.Electricity;
import priv.lhy.ecm.analysis.mapper.AbsMapper;

@Mapper
public interface ElectricityMapper extends AbsMapper<Electricity> {

}