package priv.lhy.ecm.analysis.constants;


import priv.lhy.ecm.analysis.entity.elec.Electricity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * author: lihy
 * date: 2019/6/26 16:11
 * description:
 */
public class TableInfo {

    //基础表前缀
    public static Map<Class, String> tableNamePrefix = new HashMap<>();
    //已创建的表
    public static Set<String> existTable = new HashSet<>();

    static {
        tableNamePrefix.put(Electricity.class, "em_electricity");
        //tableNamePrefix.put(WaterMeter.class, "em_watermeter");
    }


}
