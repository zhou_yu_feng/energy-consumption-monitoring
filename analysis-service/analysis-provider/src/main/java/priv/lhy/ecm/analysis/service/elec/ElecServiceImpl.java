package priv.lhy.ecm.analysis.service.elec;

import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import priv.lhy.common.abs.AbstractRequest;
import priv.lhy.common.abs.AbstractResponse;
import priv.lhy.ecm.analysis.constants.AnalysisResponseCode;
import priv.lhy.ecm.analysis.dto.elec.ElecQueryRequest;
import priv.lhy.ecm.analysis.dto.elec.ElecQueryResponse;
import priv.lhy.ecm.analysis.entity.elec.Electricity;
import priv.lhy.ecm.analysis.exception.AnalysisException;
import priv.lhy.ecm.analysis.mapper.elec.ElectricityMapper;
import priv.lhy.ecm.analysis.service.AbsService;
import priv.lhy.ecm.analysis.util.ResponseUtil;

import java.text.ParseException;
import java.util.List;

/**
 * @author: lihy
 * date: 2019/7/4 11:50
 * description:
 */
@Service(interfaceClass = IElecService.class)
@Component
public class ElecServiceImpl extends AbsService<Electricity> implements IElecService {

    private ElectricityMapper elecMapper;

    @Autowired
    public ElecServiceImpl(ElectricityMapper mapper) {
        super(mapper);
        this.elecMapper = mapper;
    }

    @Override
    public AbstractResponse selectByTime(AbstractRequest request) {
        ElecQueryRequest elecRequest = (ElecQueryRequest) request;
        ElecQueryResponse response = new ElecQueryResponse();
        try {
            List<Electricity> result = super.doSelect(elecRequest.getCompanyCode(),
                    elecRequest.getShowColumns(), elecRequest.getBegin(), elecRequest.getEnd());
            if (result.size() > 0) {
                ResponseUtil.getResponse(response, AnalysisResponseCode.SUCCESS);
                response.setData(result);
            } else {
                ResponseUtil.getResponse(response, AnalysisResponseCode.NO_DATA);
            }
        } catch (ParseException e) {
            ResponseUtil.getResponse(response, AnalysisResponseCode.SYS_PARAM_NOT_RIGHT);
        } catch (Exception ex) {
            throw new AnalysisException(AnalysisResponseCode.SYSTEM_BUSY.getCode(),
                    AnalysisResponseCode.SYSTEM_BUSY.getMsg(), request);
        }
        return response;
    }
}
