package priv.lhy.ecm.analysis.mapper;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author: lihy
 * date: 2019/7/4 10:12
 * description:
 */
public interface AbsMapper<T> {

    /**
     * 查询时间段内的数据
     * @param tableName
     * @param columns
     * @param begin
     * @param end
     * @return
     */
    List<T> selectByTime(@Param("tablename") String tableName, @Param("columns") String columns, @Param("begin") String begin, @Param("end") String end);
}
