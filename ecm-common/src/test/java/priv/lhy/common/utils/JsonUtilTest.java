package priv.lhy.common.utils;

import org.codehaus.jackson.type.TypeReference;
import org.junit.Test;
import priv.lhy.entity.Userinfo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

/**
 * author: lihy
 * date: 2019/4/12 15:40
 * description:
 */
public class JsonUtilTest {
    @Test
    public void json2Object() throws Exception {
        String json = "{\"id\": \"1\",\"username\": \"alex\",\"birthday\": \"2000-01-01\"}";
        JsonUtil.getObjectMapper().setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
        Userinfo user = JsonUtil.json2Object(json, Userinfo.class);
        System.out.println(user.toString());
    }

    @Test
    public void json2GenericObject() throws Exception {
        String json = "[{\n" +
                "\t\"id\": 1,\n" +
                "\t\"username\": \"alex\",\n" +
                "\t\"birthday\": \"2000-1-1\"\n" +
                "}, {\n" +
                "\t\"id\": 2,\n" +
                "\t\"username\": \"tom\",\n" +
                "\t\"birthday\": \"2000-2-1\"\n" +
                "}, {\n" +
                "\t\"id\": 3,\n" +
                "\t\"username\": \"mic\",\n" +
                "\t\"birthday\": \"2000-3-1\"\n" +
                "}]";
        JsonUtil.getObjectMapper().setDateFormat(new SimpleDateFormat("yyyy-MM-dd"));
        List<Userinfo> list = JsonUtil.json2GenericObject(json, new TypeReference<List<Userinfo>>() {
        });
        for (Userinfo user: list) {
            System.out.println(user.toString());
        }
    }

    @Test
    public void object2Json() throws Exception {
        Userinfo user = new Userinfo();
        user.setBirthday(new Date());
        user.setId(1);
        user.setUsername("alex");
        System.out.println(JsonUtil.Object2Json(user));

        Userinfo user2 = new Userinfo();
        user2.setBirthday(new Date());
        user2.setId(2);
        user2.setUsername("tom");

        Userinfo user3 = new Userinfo();
        user3.setBirthday(new Date());
        user3.setId(3);
        user3.setUsername("alex");

        List<Userinfo> list = new ArrayList<>();
        list.add(user);
        list.add(user2);
        list.add(user3);

        System.out.println(JsonUtil.Object2Json(list));
    }

}