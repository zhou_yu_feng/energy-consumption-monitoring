package priv.lhy.common.utils;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * @author: lihy
 * date: 2019/7/4 11:15
 * description:
 */
public class DateUtilTest {
    @Test
    public void getYearAndMonth() throws Exception {
        System.out.println(DateUtil.getYearAndMonth());
    }

    @Test
    public void dateCustomStr() throws Exception {
        System.out.println(DateUtil.dateCustomStr(new Date(), "yyyyMMdd"));
    }

    @Test
    public void interval() throws Exception {
        System.out.println(DateUtil.interval("2018-12-1 11:11:11", "2019-7-2 22:22:22"));
    }

    @Test
    public void str2Date() throws Exception {
        System.out.println(DateUtil.str2Date("2019-7-1 23:34:54", "yyyy-MM"));
    }

}