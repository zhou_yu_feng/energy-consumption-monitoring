package priv.lhy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import priv.lhy.common.constants.WebConstant;

/**
 * Unit test for simple App.
 */
public class WebConstantTest
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void WebConstantTest()
    {
        assertEquals(WebConstant.ECM_SSO_ACCESS_RUL,"http://localhost:8080/login");
    }
}
