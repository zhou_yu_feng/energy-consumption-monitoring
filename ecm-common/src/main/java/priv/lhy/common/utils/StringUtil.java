package priv.lhy.common.utils;

/**
 * @author: lihy
 * date: 2019/7/3 10:47
 * description:
 */
public class StringUtil {

    /**
     * 首字母大写
     * @param world
     * @return
     */
    public static String firstCharUpperCase(String world) {
        char[] chars = world.toCharArray();
        chars[0] -= 32;
        return new String(chars);
    }
}
