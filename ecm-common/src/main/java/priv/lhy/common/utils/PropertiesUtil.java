package priv.lhy.common.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * author: lihy
 * date: 2019/4/2 9:13
 * description: 配置文件操作类
 */
public class PropertiesUtil {

    private PropertiesUtil() {
    }

    /**
     * 获取配置文件对应值
     *
     * @param file 配置文件名
     * @param key
     * @return
     * @throws IOException
     */
    public static String getValue(String file, String key) {

        InputStream is = Object.class.getResourceAsStream("/" + file);
        Properties config = new Properties();
        try {
            config.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return config.getProperty(key);
    }
}
