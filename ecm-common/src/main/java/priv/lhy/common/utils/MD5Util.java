package priv.lhy.common.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * author: lihy
 * date: 2019/4/2 9:45
 * description: MD5加密工具类
 */
public class MD5Util {

    /**
     * 获取加密字符串
     *
     * @param str  需要加密字符串
     * @param salt 盐
     * @return
     */
    public static String getMD5Str(String str, String salt) {
        str = encrypt(str) + encrypt(salt);
        return encrypt(str);
    }

    /***
     * md5加密
     *
     * @param str
     *            加密字符串
     * @return
     */
    private static String encrypt(String str) {
        MessageDigest messageDigest = null;

        try {
            messageDigest = MessageDigest.getInstance("MD5");

            messageDigest.reset();

            messageDigest.update(str.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException caught!");
            System.exit(-1);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        byte[] byteArray = messageDigest.digest();

        StringBuffer md5StrBuff = new StringBuffer();

        for (int i = 0; i < byteArray.length; i++) {
            if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
                md5StrBuff.append("0").append(
                        Integer.toHexString(0xFF & byteArray[i]));
            else
                md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
        }

        return md5StrBuff.toString();
    }
}
