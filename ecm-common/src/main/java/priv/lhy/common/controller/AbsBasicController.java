package priv.lhy.common.controller;

import priv.lhy.common.abs.AbstractRequest;
import priv.lhy.common.abs.AbstractResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * author: lihy
 * date: 2019/6/25 10:14
 * description: 抽象controller方法，封装请求和相应参数的组装
 */
@SuppressWarnings("unchecked")
public abstract class AbsBasicController {
    /**
     * 组装请求
     * @param entityRequest
     * @param request
     * @return
     */
    protected AbstractRequest assemblyRequest(AbstractRequest entityRequest, HttpServletRequest
            request) {
        entityRequest.setUrl(request.getRequestURI());
        entityRequest.setRequestTime(new Date());
        entityRequest.setSessionId(request.getRequestedSessionId());
        return entityRequest;
    }

    /**
     * 组装相应
     * @param response
     * @return
     */
    protected Map<String, Object> assemblyResult(AbstractResponse response) {
        Map<String, Object> map = new HashMap<>();
        map.put("code", response.getCode());
        map.put("msg", response.getMsg());
        return map;
    }
}
