package priv.lhy.common.exception;

import priv.lhy.common.abs.AbstractRequest;

/**
 * author: lihy
 * date: 2019/4/4 9:37
 * description: 异常抽象类，所有自定义异常都要继承此类
 */
public abstract class AbstractException extends RuntimeException {

    private String code;
    private String msg;
    private AbstractRequest request;

    public AbstractException(String code, String msg, AbstractRequest request) {
        this.code = code;
        this.msg = msg;
        this.request = request;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public AbstractRequest getRequest() {
        return request;
    }

    public void setRequest(AbstractRequest request) {
        this.request = request;
    }
}
