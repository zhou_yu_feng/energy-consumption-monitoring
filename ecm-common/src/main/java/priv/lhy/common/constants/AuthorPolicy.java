package priv.lhy.common.constants;

/**
 * @author: lihy
 * date: 2019/7/22 18:53
 * description: 用户权限
 */
public enum AuthorPolicy {
    //管理员
    ADMIN,
    //普通用户
    USER;
}
