package priv.lhy.common.constants;

/**
 * author: lihy
 * date: 2019/6/21 14:25
 * description:redis相关信息
 */
public class RedisConstant {

    //采集程序令牌列表
    public final static String COLLECTOR_TOKEN = "COLLECTOR:TOKEN";
    //采集程序令牌验证错误次数
    public final static int COLLECTOR_IP_ERROR_TIME = 5;
    //采集程序令牌验证错误前缀
    public final static String COLLECTOR_IP_ERROR_PREFIX = "COLLECTOR:IP:ERROR";
    //非法采集程序IP黑名单前缀
    public final static String COLLECTOR_IP_BLACK_PREFIX = "COLLECTOR:IP:BLACKLIST";
    //黑名单过期时间60分钟
    public final static int COLLECTOR_BLACK_EXPIRE = 60;

}
