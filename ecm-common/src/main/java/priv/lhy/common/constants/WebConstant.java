package priv.lhy.common.constants;

import priv.lhy.common.utils.PropertiesUtil;

/**
 * @author: lihy
 * date: 2019/4/2 9:11
 * description: 统一配置站点
 */
public class WebConstant {

    //统一登录站点
    public static final String ECM_SSO_ACCESS_URL = PropertiesUtil.getValue("common-application.properties", "ecm.sso.access.url");
    //前台展示站点
    public static final String ECM_ANALYSIS_URL  = PropertiesUtil.getValue("common-application.properties", "ecm.analysis.index");
    //后台管理站点
    public static final String ECM_MANAGE_URL  = PropertiesUtil.getValue("common-application.properties", "ecm.manage.index");
}
