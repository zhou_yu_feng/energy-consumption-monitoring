package priv.lhy.common.constants;

/**
 * @author: lihy
 * date: 2019/5/13 11:22
 * description: service通用方法类型
 */
public enum MethodCode {
    //新增
    INSERT,
    //修改
    MODIFY;
}
