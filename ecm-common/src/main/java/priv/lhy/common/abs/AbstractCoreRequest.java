package priv.lhy.common.abs;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: lihy
 * date: 2019/4/2 12:57
 * description: 请求抽象类，所有请求必须继承此类
 */
public class AbstractCoreRequest<T> extends AbstractRequest implements Serializable {

    private T t;

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }
}
