package priv.lhy.common.abs;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: lihy
 * date: 2019/4/2 13:08
 * description: 抽象相应类，所有相应必须继承此类
 */
public class AbstractResponse implements Serializable {

    private static final long serialVersionUID = 6066376110298197952L;

    //响应码
    private String code;
    //响应信息
    private String msg;
    //请求用户编号
    private int uid;
    //请求session编号
    private String sessionId;
    //响应时间
    private Date responseTime;
    //返回自定义对象
    private Object data;
    //返回分页记录总数
    private int count;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Date getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Date responseTime) {
        this.responseTime = responseTime;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
