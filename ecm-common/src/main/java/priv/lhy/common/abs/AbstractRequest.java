package priv.lhy.common.abs;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: lihy
 * date: 2019/4/2 12:57
 * description: 请求抽象类，所有请求必须继承此类
 */
public class AbstractRequest implements Serializable {

    private static final long serialVersionUID = -4002551601754930412L;


    //请求用户编号
    private int uid;

    //用户权限编号
    private Long accessId;

    //请求session编号
    private String sessionId;

    //请求时间
    private Date requestTime;

    //请求路径
    private String url;


    public AbstractRequest() {
        this.requestTime = new Date();
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public Long getAccessId() {
        return accessId;
    }

    public void setAccessId(Long accessId) {
        this.accessId = accessId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Date getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(Date requestTime) {
        this.requestTime = requestTime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
